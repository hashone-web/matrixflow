var Role = require("../model/roles");
var RoleHelper = require('../helpers/role');

module.exports = {
    view: async (req, res, next) => {
        try {
            var Helper = await RoleHelper.getAllRole();
            if (Helper.length > 0) {
                res.send(Helper);
            } else {
                res.send("No Permission found");
            }
        } catch (error) {
            console.log("Role View Error : ", error);
            res.send(error);
        }
    },

    create: (req, res, next) => {
        try {
            var role = new Role(req.body);
            role.save()
                .then(result => {
                    res.send(result);
                }).catch(err => {
                    res.send(err);
                })
        } catch (error) {
            console.log("Role Create Error : ", error);
            res.send(error);
        }
    },

    edit: async (req, res, next) => {
        try {
            var query = { _id: req.params.id }
            var Helper = await RoleHelper.getRoleById(query);
            if (Helper) {
                res.send(Helper);
            } else {
                res.send("No Permission found");
            }
        } catch (error) {
            console.log("Role Edit Error : ", error);
            res.send(error);
        }
    },

    update: async (req, res, next) => {
        try {
            var con = { _id: req.params.id };
            var query = { name: req.body.name, permissions: req.body.permissions, updated_at: Date.now() }
            var Helper = await RoleHelper.findAndUpdate(con, query);
            if (Helper) {
                res.send(Helper);
            } else {
                res.send("No Permission updated");
            }
        } catch (error) {
            console.log("Role Update Error : ", error);
            res.send(error);
        }
    },

    destroy: async (req, res, next) => {
        try {
            var con = { _id: req.params.id };
            var query = { deleted_at: Date.now() };
            var Helper = await RoleHelper.findAndUpdate(con, query);
            if (Helper) {
                res.send(Helper);
            } else {
                res.send("No Permission updated");
            }
        } catch (error) {
            console.log("Role Destory Error : ", error);
            res.send(error);
        }
    }
}