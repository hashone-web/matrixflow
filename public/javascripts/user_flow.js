$(document).ready(function () {
    $(".destroy").click(destroy);
    $('.select2-single').select2({
    });
});

function destroy() {
    let token = $("#csrf").val();
    var user_id = $("#user_id").val();
    var par = $(this).parent().parent();
    var id = par.children("td:nth-child(1)");

    $.confirm({
        'boxWidth': '500px',
        'useBootstrap': false,
        'title': 'Delete Confirmation',
        'message': 'You are about to delete this item. <br />It cannot be restored at a later time! Continue?',
        'buttons': {
            'Yes': {
                'class': 'blue',
                'action': function () {
                    jQuery.ajax({
                        type: 'POST',
                        url: `/user-flows/destroy`,
                        data: {
                            _csrf: token,
                            user_id: user_id,
                            flow_id: id.html()
                        },
                        success: function (response) {
                            window.location.href = window.location.origin + '/user-flows/' + user_id
                        },
                    });
                }
            },
            'No': {
                'class': 'gray',
                'action': function () {
                    jQuery.ajax({
                        type: 'GET',
                        url: '/users'
                    });
                }
            }
        }
    });
};