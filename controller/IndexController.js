var passport = require('../config/passport');
var auth = require('../config/auth');
module.exports = {

    getSignin: (req, res, next) => {
        try {
            res.render('index', {
                title: 'Sign In',
                what: 'pages/signin',
                csrftoken: req.token,
                user_data: ''
            });
        } catch (error) {
            console.log("getLogin error : ", error);
            res.render('index');
        }
    },

    login: (req, res, next) => {
        if (req.body._csrf == undefined || req.body._csrf == null) {
            res.redirect('index', {
                what: 'error',
                message: "csrf mismatch",
                error: {
                    status: 500,
                    stack: 'CSRF'
                }
            });
        } else {
            passport.authenticate('local-login', (err, user, info) => {
                if (err) {
                    return res.render('index',
                        {
                            what: 'pages/signin',
                            title: 'Login',
                            user_data: err,
                            csrftoken: req.body._csrf
                        });
                } else {
                    if (user.error) {
                        return res.render('index', {
                            what: 'pages/signin',
                            title: 'Login',
                            user_data: user,
                            csrftoken: req.body._csrf
                        });
                    } else {
                        req.flash(
                            'success',
                            `Successfully Login`
                        );
                        req.logIn(user, async (err) => {
                            req.session.permissions = await auth.permissionCheck(req.session);
                            if (err) { return next(err); }
                            return res.redirect('/flows');
                        });
                    }
                }
            })(req, res, next);
        }
    },

    logout: (req, res, next) => {
        try {
            req.logout();
            // req.session.destroy();
            res.redirect('/signin');
            next();
        } catch (error) {
            console.log("logout error : ", error);
            res.redirect('/');
        }
    }
}