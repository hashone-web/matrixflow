$(document).ready(function () {
    var url = window.location.href;
    var arr = url.split("/");
    var result = arr[0] + "//" + arr[2] + "/";
    var counter = 0;
    var isImage = false;
    image_crop_edit = $('#upload-image-user').croppie({
        url: result + $("#profile_picture_user").val(),
        enableExif: true,
        viewport: {
            width: 200,
            height: 200,
            type: 'square'
        },
        boundary: {
            width: 300,
            height: 300
        },
        update: function () {
            if (counter == 0) {
                counter++;
                $('#upload-image-user').croppie('setZoom', '0');
            }
        }
    });
    image_crop = $('#upload-image').croppie({
        url: '/image-placeholder.jpg',
        enableExif: true,
        enableOrientation: true,
        viewport: {
            width: 200,
            height: 200,
            type: 'square'
        },
        boundary: {
            width: 300,
            height: 300
        },
        update: function () {
            if (counter == 0) {
                counter++;
                $('#upload-image').croppie('setZoom', '0');
            }
        }
    });
    $('#images').on('change', function () {
        var validExts;
        if ($("#upload-image").data('accept') !== undefined) {
            validExts = $("#upload-image").data('accept');
        }
        if ($("#upload-image-user").data('accept') !== undefined) {
            validExts = $("#upload-image-user").data('accept');
        }
        // var validExts = $("#upload-image").data('accept');
        var file = this.files[0];
        var fileExt = file.name.substring(file.name.lastIndexOf('.'));
        if (validExts.indexOf(fileExt) < 0) {
            alert("Invalid file selected, valid files are of " +
                validExts.toString() + " types.");
            $("#upload-image").css("border - color", "#DD1414");
            $("#upload-image - error").text("Only " + validExts.toString() + " Files allowed");
            return false;
        } else {
            var reader = new FileReader();

            reader.onload = function (e) {
                if ($("#upload-image").data('accept') !== undefined) {
                    image_crop.croppie('bind', {
                        url: e.target.result,
                    }).then(function () {
                        isImage = true;
                    });
                }
                if ($("#upload-image-user").data('accept') !== undefined) {
                    image_crop_edit.croppie('bind', {
                        url: e.target.result,
                    }).then(function () {
                        isImage = true;
                        // console.log('jQuery bind complete');
                    });
                }
            }
            reader.readAsDataURL(this.files[0]);
            return true;
        }
    });

    $('#user_form').on('submit', function (e) {
        e.preventDefault();
        if ($("#upload-image").data('accept') !== undefined) {
            image_crop.croppie('result', {
                type: 'canvas',
                size: 'viewport',
            }).then(function (response) {
                if(isImage == true) {
                    $('#picture').val(response);
                }
                $('#user_form').unbind().submit();
            });

        }
        if ($("#upload-image-user").data('accept') !== undefined) {
            image_crop_edit.croppie('result', {
                type: 'canvas',
                size: 'viewport',
            }).then(function (response) {
                $('#picture').val(response);
                $('#user_form').unbind().submit();
            });
        }
    });

    $('.alphaspace-only').keypress(validateAlphaspace)
    $('.numeric-only').keypress(validateNumber)

    function validateAlphaspace(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 32 || (key >= 65 && key <= 90) || (key >= 97 && key <= 122)) {
            return true;
        }
        return false;
    };

    function validateNumber(event) {
        var key = window.event ? event.keyCode : event.which;
        if (event.keyCode === 8 || event.keyCode === 46) {
            return true;
        } else if (key < 48 || key > 57 || (key >= 65 && key <= 90)) {
            return false;
        } else {
            return true;
        }
    };
});