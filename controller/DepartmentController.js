var Department = require("../model/department");
const { compareSync } = require("bcryptjs");

module.exports = {
    view: async (req, res, next) => {
        try {
            if (req.user.role.name === 'Admin') {
                var query = {};
                query['deleted_at'] = null;
                var perPage = req.query.limit ? JSON.parse(req.query.limit) : 5;
                var page = req.query.page ? req.query.page : 1;
                var sort_by = req.query.sort_by ? req.query.sort_by : '_id';
                var sort_type = req.query.sort_type ? req.query.sort_type : 'desc';
                // for search query 
                if (req.query.search_string) {
                    query['name'] = new RegExp(req.query.search_string, "ig");
                }

                // for filter
                if (req.query.filter_status) {
                    query['status'] = req.query.filter_status;
                    filter_status = req.query.filter_status;
                }
                await Department.find(query)
                    .sort([[sort_by, sort_type]])
                    .skip((perPage * page) - perPage)
                    .limit(perPage)
                    .exec((err, department) => {
                        Department.countDocuments(query).exec(function (err, count) {
                            if (err) return next(err);
                            else {
                                res.render('logedin_index', {
                                    what: 'pages/department/index',
                                    title: 'Department Pages',
                                    page_name: 'departments',
                                    user: req.user,
                                    list: department,
                                    current: page,
                                    sort_by: sort_by,
                                    sort_type: sort_type,
                                    filter_status: req.query.filter_status,
                                    search_string: req.query.search_string,
                                    pages: Math.ceil(count / perPage),
                                    status: Department.schema.path('status').enumValues,
                                    success: req.flash('success'),
                                    csrftoken: req.token,
                                    // moment: moment,
                                });
                            }
                        })
                    })
            } else {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To View Departments',
                    href: req.baseUrl,
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'departments',
                });
            }
        } catch (error) {
            console.log("Department View Error : ", error);
            res.redirect('/');
        }
    },

    create: async (req, res, next) => {
        try {
            if (req.user.role.name === 'Admin') {
                req.active = req.path.split('/')[1];
                res.render('logedin_index', {
                    what: 'pages/department/create',
                    title: 'Create Department',
                    csrftoken: req.token,
                    user: req.user,
                    data: '',
                    link: req.active,
                    error: '',
                    created_by: req.user._id,
                    status: Department.schema.path('status').enumValues,
                    page_name: 'departments',
                });
            } else {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To Create Department',
                    href: req.baseUrl,
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'departments',
                });
            }

        } catch (error) {
            console.log("deartement create error :: ", error);
            res.redirect('/');
        }
    },

    store: (req, res, next) => {
        try {
            var department = new Department(req.body);
            department.save()
                .then(result => {
                    req.flash(
                        'success',
                        `Department Successfully Created`
                    );
                    res.redirect('/departments');
                }).catch(err => {
                    req.flash(
                        'info',
                        `Something Went Wron When Create Department`
                    );
                    res.redirect('/departments');
                })
        } catch (error) {
            console.log("Department Create Error : ", error);
            res.redirect('/');
        }
    },

    edit: async (req, res, next) => {
        try {
            if (req.user.role.name === 'Admin') {

                var referer = req.headers.referer.split('departments')[1];
                var department = await commonFindOneData('Department', { _id: req.params.id, deleted_at: null });
                if (!department.errorMessage) {
                    req.session['redirect'] = referer;
                    req.active = req.path.split('/')[2];
                    res.render('logedin_index', {
                        what: 'pages/department/create',
                        title: 'Edit Department',
                        csrftoken: req.token,
                        user: req.user,
                        data: department,
                        link: req.active,
                        error: '',
                        created_by: req.user._id,
                        status: Department.schema.path('status').enumValues,
                        page_name: 'departments',
                    });
                } else {
                    req.flash(
                        'info',
                        `Something Went Wron When Edit Department`
                    );
                    res.redirect('/departments');
                }
            } else {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To Edit Department',
                    href: req.baseUrl,
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'departments',
                });
            }
        } catch (error) {
            console.log("Department Edit Error : ", error);
            res.redirect('/');
        }
    },

    update: async (req, res, next) => {
        try {
            var con = { _id: req.params.id };
            var query = { name: req.body.name, status: req.body.status, updated_at: Date.now() }
            var department = await commonFindOneAndUpdateData('Department', con, query);
            if (!department.errorMessage) {
                req.flash(
                    'success',
                    `Department Successfully Updated`
                );
                if(req.session.redirect) {
                    res.redirect('/departments/' + req.session.redirect);
                } else {
                    res.redirect('/departments');
                }
            } else {
                req.flash(
                    'info',
                    `Something Went Wron When Update Department`
                );
                res.redirect('/departments');
            }
        } catch (error) {
            console.log("Department Edit Error : ", error);
            res.redirect('/');
        }
    },

    destroy: async (req, res, next) => {
        try {
            var con = { _id: req.params.id };
            var query = { deleted_at: Date.now() };
            var department = await commonFindOneAndUpdateData('Department', con, query);
            if (!department.errorMessage) {
                req.flash(
                    'success',
                    `Department Successfully Destory!`
                );
                res.status(200).send(department)
            } else {
                req.flash(
                    'info',
                    `Something Went Wron When Delete Department`
                );
                res.redirect('/departments'); s
            }
        } catch (error) {
            console.log("Department Edit Error : ", error);
            res.redirect('/');
        }
    },

    status: (req, res, next) => {
        try {
            Department.findByIdAndUpdate(req.body.id, {
                status: req.body.status,
                updated_at: Date.now()
            }, {
                new: false
            })
                .then(department => {
                    req.flash(
                        'success',
                        `Department Status Change Successfully`
                    );
                    res.json({
                        status: true,
                    });
                })
                .catch(error => {
                    res.json({
                        status: false,
                    });
                })
        } catch (error) {

            res.json({
                status: false,
            });
        }
    },
}