var express = require('express');
var router = express.Router();
var IndexController = require('../controller/IndexController');
var auth = require("../config/auth");
var UserHelper = require('../helpers/user');

/* GET home page. */
router.get('/', async (req, res, next) => {
    if (req.user) {
        req.user = await UserHelper.findOnePopulate({ _id: req.user._id });
        if (req.isAuthenticated()) {
            req.session.permissions = await auth.permissionCheck(req.session);
            // if (err) { return next(err); }
            return res.redirect('/flows');
        } else {
            res.render('index', { what: 'index', title: 'Welcome' });
        }
    } else {
        res.render('index', { what: 'index', title: 'Welcome' });
    }
});

router.get('/signin', [auth.csrfConfig], IndexController.getSignin);

router.post('/signin', IndexController.login);

router.get('/logout', IndexController.logout);

module.exports = router;