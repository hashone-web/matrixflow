var express = require('express');
var app = express();
var chokidar = require('chokidar');
var watcher = chokidar.watch('./app');
watcher.on('ready', function () {
    watcher.on('all', function () {
        console.log("Clearing /dist/ module cache from server")
        Object.keys(require.cache).forEach(function (id) {
            console.log("require.cache[id] --> ", require.cache[id]);
            if (/[\/\\]app[\/\\]/.test(id)) delete require.cache[id]
        })
    })
})

chokidar.watch('./routes').on('all', (event, at) => {
    if (event === 'add') {
        debug('Watching for', at);
    } if (event === 'change') {
        debug('Changes at', at);
        restart(); // assume that this exists
    }
});
var POST = 5000;
app.listen(POST, () => {
    console.log("server port : ", POST)
});

function restart() {
    console.log("require.cache ---> ", require.cache);
    // clean the cache
    Object.keys(require.cache).forEach((id) => {
        if (pathCheck(id)) {
            debug('Reloading', id);
            delete require.cache[id];
        }
    });
}