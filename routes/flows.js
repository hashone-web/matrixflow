var express = require('express');
var router = express.Router();
var FlowController = require('../controller/FlowController');
var auth = require('../config/auth');

// flow/
router.get('/', [auth.isLoggedIn, auth.csrfConfig], FlowController.view);

// flow/create
router.get('/create', [auth.isLoggedIn, auth.csrfConfig], FlowController.create);

router.post('/points', [auth.isLoggedIn, auth.csrfConfig], FlowController.create_points);

// flow/store
router.post('/', [auth.isLoggedIn], FlowController.store);

// flow/status
router.post('/status', [auth.isLoggedIn], FlowController.status);

// flow/:id/edit
router.get('/:id/edit', [auth.isLoggedIn, auth.csrfConfig], FlowController.edit);

// flow/:id
router.post('/:id', [auth.isLoggedIn], FlowController.update);

// flow/:id/destory
router.post('/:id/destroy', [auth.isLoggedIn], FlowController.destroy);

// flow/:id/points
router.get('/:id/sorting', [auth.isLoggedIn], FlowController.sorting);

// flow/:id/points
router.post('/:id/points', [auth.isLoggedIn, auth.csrfConfig], FlowController.updatePoints);

// flow/:slug_name/points
router.get('/:id/points', [auth.isLoggedIn], FlowController.points);

module.exports = router;
