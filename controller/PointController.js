var Point = require("../model/point");

function getUniqueId() {
    return (new Date().getTime()).toString(36) + new Date().getUTCMilliseconds();
}

module.exports = {
    view: async (req, res, next) => {
        try {
            if (req.user.role.name === 'Admin') {
                var query = {};
                query['deleted_at'] = null;
                var flow_filter = '';
                var perPage = req.query.limit ? JSON.parse(req.query.limit) : 5;
                var page = req.query.page ? req.query.page : 1;
                var sort_by = req.query.sort_by ? req.query.sort_by : 'created_at';
                var sort_type = req.query.sort_type ? req.query.sort_type : 'desc';
                var flow = await commonFindData('Flow', { deleted_at: null, status: 'active' });
                // for search query 
                if (req.query.search_string) {
                    query['title'] = new RegExp(req.query.search_string, "ig");
                }

                // for filter
                if (req.query.filter_status) {
                    query['status'] = req.query.filter_status;
                    filter_status = req.query.filter_status;
                }
                // for department filter
                if (req.query.flow_filter) {
                    query['flow'] = req.query.flow_filter;
                    flow_filter = req.query.flow_filter;
                }
                await Point.find(query)
                    .populate('flow')
                    .sort([[sort_by, sort_type]])
                    .skip((perPage * page) - perPage)
                    .limit(perPage)
                    .exec(async (err, point) => {
                        Point.countDocuments(query).exec(function (err, count) {
                            if (err) return next(err);
                            else {
                                res.render('logedin_index', {
                                    what: 'pages/points/index',
                                    title: 'Points Pages',
                                    page_name: 'points',
                                    user: req.user,
                                    list: point,
                                    flow: flow,
                                    current: page,
                                    sort_by: sort_by,
                                    sort_type: sort_type,
                                    filter_status: req.query.filter_status,
                                    search_string: req.query.search_string,
                                    flow_filter: flow_filter,
                                    pages: Math.ceil(count / perPage),
                                    status: Point.schema.path('status').enumValues,
                                    success: req.flash('success'),
                                    csrftoken: req.token,
                                    // moment: moment,
                                });
                            }
                        })
                    })
            } else {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To View Points',
                    href: req.baseUrl,
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'points',
                });
            }

        } catch (error) {
            console.log("Flow View Error : ", error);
            res.redirect('/');
        }
    },

    create: async (req, res, next) => {
        try {
            if (req.user.role.name === 'Admin') {
                var data = (req.session.flow) ? { flow: req.session.flow } : '';
                var media_key = getUniqueId();
                var flow = await commonFindData('Flow', { deleted_at: null, status: 'active' });
                req.active = req.path.split('/')[1];
                res.render('logedin_index', {
                    what: 'pages/points/create',
                    title: 'Create Points',
                    csrftoken: req.token,
                    user: req.user,
                    media_key: media_key,
                    data: data,
                    link: req.active,
                    error: '',
                    flow: flow,
                    created_by: req.user._id,
                    status: Point.schema.path('status').enumValues,
                    page_name: 'points',
                    success: req.flash ? req.flash('success') : ''
                });
            } else {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To Create Points',
                    href: req.baseUrl,
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'points',
                    success: req.flash ? req.flash('success') : ''
                });
            }
        } catch (error) {
            console.log("Points create error :: ", error);
            res.redirect('/');
        }
    },

    store: async (req, res, next) => {
        try {
            await Point.find({ deleted_at: null, flow: req.body.flow })
                .sort([['version', -1]])
                .collation({ locale: "en_US", numericOrdering: true })
                .then(result => {
                    var last_version = (result.length > 0) ? result[0].version.split(".") : '0.0';
                    req.body.version = (Number(last_version[0]) + 1) + '.0.0';
                    // req.body.slug = string_to_slug(req.body.title);
                    req.body.hints = (req.body.hints) ? req.body.hints : null;
                    req.body.content = req.body.content.replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>', '');
                    var point = new Point(req.body);
                    point.save()
                        .then(result => {
                            req.flash(
                                'success',
                                `Point Successfully Created`
                            );
                            res.redirect('/points/create');
                        }).catch(err => {
                            console.log("point Create Error : ", err);
                            res.redirect('/');
                        })
                })
                .catch(err => {
                    req.flash(
                        'Info',
                        `Error While Point Created`
                    );
                    console.log("point Create Error : ", err);
                    res.redirect('/points/create');
                })

        } catch (error) {
            console.log("point Create Error : ", error);
            res.redirect('/');
        }
    },

    edit: async (req, res, next) => {
        try {
            if (req.user.role.name === 'Admin') {
                var referer = req.headers.referer.split('flows/')[1];
                // var referer = req.headers.referer;
                var flow = await commonFindData('Flow', { deleted_at: null, status: 'active' });
                var points = await commonFindOneData('Point', { _id: req.params.id, deleted_at: null });
                if (!flow.errorMessage) {
                    req.active = req.path.split('/')[2];
                    req.session['redirect'] = referer;
                    res.render('logedin_index', {
                        what: 'pages/points/create',
                        title: 'Edit Points',
                        csrftoken: req.token,
                        user: req.user,
                        media_key: points.media_key,
                        data: points,
                        link: req.active,
                        error: '',
                        created_by: req.user._id,
                        status: Point.schema.path('status').enumValues,
                        flow: flow,
                        page_name: 'points',
                        success: req.flash ? req.flash('success') : ''
                    });
                } else {
                    req.flash(
                        'info',
                        `Something Went Wron When Edit Points`
                    );
                    res.redirect('/points');
                }
            } else {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To Edit Points',
                    href: req.baseUrl,
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'points',
                    success: req.flash ? req.flash('success') : ''
                });
            }
        } catch (error) {
            console.log("Points Edit Error : ", error);
            res.redirect('/');
        }
    },

    update: async (req, res, next) => {
        try {
            // req.body.slug = string_to_slug(req.body.title);
            req.body.hints = (req.body.hints) ? req.body.hints : null;
            req.body.content = req.body.content.replace('<p data-f-id="pbf" style="text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;">Powered by <a href="https://www.froala.com/wysiwyg-editor?pb=1" title="Froala Editor">Froala Editor</a></p>', '');
            var con = { _id: req.params.id, deleted_at: null };
            var find = await commonFindOneData('Point', con);
            if (find.flow != req.body.flow) {
                await Point.find({ deleted_at: null, flow: req.body.flow })
                    .sort([['version', -1]])
                    .collation({ locale: "en_US", numericOrdering: true })
                    .then(async result => {
                        var last_version = (result.length > 0) ? result[0].version.split(".") : '0.0';
                        req.body.version = await (Number(last_version[0]) + 1) + '.0';
                    })
                    .catch(err => {
                        req.flash(
                            'Info',
                            `Error While Flow Update`
                        );
                        console.log("err :: ", err);
                        res.redirect('/points');
                    })
            }
            var query = { title: req.body.title, content: req.body.content, slug: req.body.slug, status: req.body.status, flow: req.body.flow, hints: req.body.hints, updated_at: Date.now() }
            if (req.body.version) {
                query['version'] = req.body.version;
            }
            var point = await commonFindOneAndUpdateData('Point', con, query);
            if (!point.errorMessage) {
                req.flash(
                    'success',
                    `Points Successfully Updated`
                );
                res.redirect('/flows/' + req.session.redirect);
            } else {
                req.flash(
                    'info',
                    `Something Went Wron When Update Points`
                );
                res.redirect('/points');
            }
        } catch (error) {
            console.log("Point Edit Error : ", error);
            res.redirect('/');
        }
    },

    destroy: async (req, res, next) => {
        try {
            var con = { _id: req.params.id };
            var query = { deleted_at: Date.now() };
            var point = await commonFindOneAndUpdateData('Point', con, query);
            if (!point.errorMessage) {
                res.status(200).send(point)
            } else {
                res.status(500).send(point.errorMessage)
            }
        } catch (error) {
            console.log("Point Edit Error : ", error);
            res.status(500).send(error)
        }
    },

    status: (req, res, next) => {
        try {
            Point.findByIdAndUpdate(req.body.id, {
                status: req.body.status,
                updated_at: Date.now()
            }, {
                new: false
            })
                .then(point => {
                    req.flash(
                        'success',
                        `Points Status Change Successfully`
                    );
                    res.json({
                        status: true,
                    });
                })
                .catch(error => {
                    res.json({
                        status: false,
                    });
                })
        } catch (error) {
            res.json({
                status: false,
            });
        }
    },
}