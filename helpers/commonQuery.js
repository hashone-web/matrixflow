var mongoose = require("mongoose");

async function commonFindByIdData(collection, id) {
    try {
        var result = await mongoose.model(collection).findById(id);
        if (result == null) {
            return {
                errorMessage: "No record found"
            };
        }
        return result;
    } catch (error) {
        return {
            errorMessage: error
        };
    }
}
global.commonFindByIdData = commonFindByIdData;

async function commonFindOneData(collection, query) {
    try {
        var result = await mongoose.model(collection).findOne(query);
        if (result == null) {
            return {
                errorMessage: "No record found"
            };
        }
        return result;
    } catch (error) {
        return {
            errorMessage: error
        };
    }
}
global.commonFindOneData = commonFindOneData;

async function commonFindOnePopulateData(collection, query, populate) {
    try {
        var result = await mongoose.model(collection).findOne(query).populate(populate[0]).populate(populate[1]).populate(populate[2]);
        if (result == null) {
            return {
                errorMessage: "No record found"
            };
        }
        return result;
    } catch (error) {
        return {
            errorMessage: error
        };
    }
}
global.commonFindOnePopulateData = commonFindOnePopulateData;

async function commonFindData(collection, query) {
    try {
        var result = await mongoose.model(collection).find(query);
        if (result == null) {
            return {
                errorMessage: "No record found"
            };
        }
        return result;
    } catch (error) {
        return {
            errorMessage: error
        };
    }
}
global.commonFindData = commonFindData;

// find one and update 
async function commonFindOneAndUpdateData(collection, condition, query) {
    try {
        var result = await mongoose.model(collection).findOneAndUpdate(condition, query, { new: true });
        if (result == null) {
            return {
                errorMessage: 'No record found'
            }
        }
        return result;
    } catch (error) {
        return {
            errorMessage: error
        };
    }
};
global.commonFindOneAndUpdateData = commonFindOneAndUpdateData;