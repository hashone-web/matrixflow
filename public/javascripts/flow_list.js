$(document).ready(function () {
    var order = '';
    $(".btn-edit").click(edit);
    $(".destroy").click(destroy);
    $('[data-toggle="tooltip"]').tooltip({
        trigger : 'hover'
    })
    // $("#search").click(search);
    $("#filter_status").on('change', filter);
    $("#Department_filter").on('change', department);
    $('#search_string').keypress(function (event) {
        var keycode = (event.keyCode ? event.keyCode : event.which);
        if (keycode == '13') {
            var searchString = $("#search_string").val();
            jQuery.ajax({
                type: 'get',
                url: `flow?page=1&search_string=` + searchString,
                success: function (response) {
                    if (response.length > 0) {
                        $.alert({
                            title: 'Alert!',
                            content: 'NO FLOW FOUND',
                        });
                    } else {
                        window.location.href = window.location.origin + '/flows'
                    }
                }
            });
            window.location.href = window.location.origin + `/flows?page=1&search_string=` + searchString;
        }
    });

    $(".sorting").click(function (e) {
        if ($(this).hasClass('asc')) {
            if ($(this).children("i")) {
                $(this).children("i").removeClass("fa fa-chevron-up");
                $(this).children("i").addClass("fa fa-chevron-down");
            }
            $("th").removeClass("asc");
            $("th").removeClass("desc");
            $(this).addClass("desc");
            order = 'desc';
        } else {
            if ($(this).children("i")) {
                $(this).children("i").removeClass("fa fa-chevron-down");
                $(this).children("i").addClass("fa fa-chevron-up");
            }
            $("th").removeClass("desc");
            $(this).addClass("asc");
            order = 'asc';
        }
        var sort_by = $(this).html().toLowerCase();
        var sort = sort_by.split("<");

        jQuery.ajax({
            type: 'GET',
            url: `/flows?page=1&sort_by=` + sort[0].trim() + `&sort_type=` + order,
            success: function (response) {
                window.location.href = window.location.origin + `/flows?page=1&sort_by=` + sort[0].trim() + `&sort_type=` + order;
            }
        });
    });

    $(document).click(function () {
        $('.job-change-status-dropdown').hide();
    });

    $(".add_points").on('click', function (e) {
        var flow_id = $(this).data('flow-id')
        let token = $("#csrf").val();
        jQuery.ajax({
            type: 'POST',
            url: `/flows/points`,
            data: {
                "flow_id": flow_id,
                "_csrf": token
            },
            success: function (response) {
                window.location.href = window.location.origin + `/points/create`;
            }
        });
    });

    $('.change-job-status-btn-wrap').on('click', function (e) {
        $('.job-change-status-dropdown').hide();
        e.preventDefault();
        e.stopPropagation();
        var job_id = $(this).data('job-id');
        $('.job-change-status-dropdown[data-id="job--' + job_id + '-change-status"]').show();
    });

    $('.change-job-status').on('click', function (e) {
        var job_id = $(this).data('job-id');
        var job_status = $(this).data('job-status');
        let token = $("#csrf").val();
        $(window).scrollTop(1.1);

        $.confirm({
            animation: 'scale',
            closeAnimation: 'scale',
            title: 'Job Status Change!',
            content: 'Are you sure?',
            buttons: {
                cancel: {
                    btnClass: 'btn-warning',
                    action: function () {
                        $('.job-change-status-dropdown').hide();
                    }
                },
                confirm: {
                    btnClass: 'btn-primary',
                    action: function () {
                        $.ajax({
                            url: "/flows/status",
                            type: 'POST',
                            data: {
                                "status": job_status,
                                "id": job_id,
                                "_csrf": token
                            },
                            success: function (data) {
                                if (data.status) {
                                    location.reload();
                                } else {
                                    alert(data.message);
                                    $('.job-change-status-dropdown').hide();
                                }
                            }
                        });
                    }
                }
            }
        });
    });
});

function edit() {
    var par = $(this).parent().parent();
    let id = par.children("td:nth-child(1)");
    window.location.href = window.location.origin + `/flows/` + id.html() + `/edit`;
};

function destroy() {
    let token = $("#csrf").val();
    // console.log("token --> ", token);
    var par = $(this).parent().parent();
    var id = par.children("td:nth-child(1)");
    // console.log(id.html());
    $.confirm({
        'boxWidth': '500px',
        'useBootstrap': false,
        'title': 'Delete Confirmation',
        'message': 'You are about to delete this item. <br />It cannot be restored at a later time! Continue?',
        'buttons': {
            'Yes': {
                'class': 'blue',
                'action': function () {
                    jQuery.ajax({
                        type: 'POST',
                        url: `/flows/` + id.html() + `/destroy`,
                        data: {
                            _csrf: token
                        },
                        success: function (response) {
                            // console.log("response :: ", response);
                            window.location.href = window.location.origin + '/flows'
                        },
                    });
                }
            },
            'No': {
                'class': 'gray',
                'action': function () {
                    jQuery.ajax({
                        type: 'GET',
                        url: '/flows'
                    });
                }
            }
        }
    });
};

function search() {
    var searchField = $("#search_field").val();
    if (searchField == 'all') {
        $.alert({
            title: 'Alert!',
            content: 'SELECT FIELD TO SEARCH..',
        });
    } else {
        var searchString = $("#search_string").val();
        if (searchString.length <= 0) {
            $.alert({
                title: 'Alert!',
                content: 'INSERT SEARCH STRING TO SEARCH',
            });
        } else {
            jQuery.ajax({
                type: 'get',
                url: `flow?page=1&search_field=` + searchField + `&search_string=` + searchString,
                success: function (response) {
                    if (response.length > 0) {
                        $.alert({
                            title: 'Alert!',
                            content: 'NO USER FOUND',
                        });
                    } else {
                        window.location.href = window.location.origin + '/flows/1'
                    }
                }
            });
            window.location.href = window.location.origin + `/flows?page=1&search_field=` + searchField + `&search_string=` + searchString;
        }
    }
};

function department() {
    var DepartmentFilter = $("#Department_filter").val();
    if (DepartmentFilter == 'all') {
        window.location.href = window.location.origin + '/flows?page=1'
    } else {
        jQuery.ajax({
            type: 'get',
            url: `flow?page=1&department_filter=` + DepartmentFilter,
            success: function (response) {
                // window.location.href = window.location.origin + '/flows/1'
            }
        });
        window.location.href = window.location.origin + `/flows?page=1&department_filter=` + DepartmentFilter;
    }
}

function filter() {
    var filterStatus = $("#filter_status").val();
    if (filterStatus == 'all') {
        window.location.href = window.location.origin + '/flows?page=1'
    } else {
        jQuery.ajax({
            type: 'get',
            url: `flow?page=1&filter_status=` + filterStatus,
            success: function (response) {
                // console.log("response --> ", response)
                // window.location.href = window.location.origin + '/flows/1'
            }
        });
        window.location.href = window.location.origin + `/flows?page=1&filter_status=` + filterStatus;
    }
}