function validation() {
    if ($("#password").val() != $("#cpassword").val()) {
        $("#cpassword").addClass('is-invalid');
        $("#cp").html('<span class="text-danger">Password and confirm password not matched!</span>');
        return false;
    }
}
