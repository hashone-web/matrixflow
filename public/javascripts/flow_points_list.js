$(document).ready(function () {
    $("img.fr-fic.fr-dib").addClass('img-thumbnail');
    var header = $('.navbar-header-fixed').height();
    var ol = $("ol").height();
    var finalHeight = header + 40 + ol + 15;
    // 40 = 20 top , 20 bottom padding
    // 15 breadcurms bottom padding
    $("#myScrollspy").css("top", finalHeight);

    /* if (window.matchMedia('(min-width: 1024px)').matches) {
        $('#myScrollspy').affix({ offset: { top: 150 } });
    } else {
        $('#myScrollspy').affix({ offset: { top: 250 } });
    } */

    // $("ul.flex-row li:first a").addClass("active");
    $("ul.flex-row li a.active").parent().find('ul').show();
    $('a[href^="#"]').bind('click', function (e) {
        e.preventDefault(); // prevent hard jump, the default behavior

        var target = $(this).attr("href"); // Set the target as variable
        // perform animated scrolling by getting top-position of target-element and set it as scroll target
        $('html, body').animate({
            scrollTop: $(target).offset().top - finalHeight
        }, 100);
        return false;
    });

    jQuery(window).on('scroll', function (e) {
        $('li.parent>a').each(function () {
            if ($(this).hasClass('active')) {
                $("ul.flex-row li ul").hide();
                $(this).addClass('active');
                $(this).parent().find('ul').show();
                if ($(this).parent().children('ul')) {
                    $("li.parent > ul").hide();
                    $("li.child > ul").hide();
                    $(this).parent().children('ul').css('display', 'block');
                    $(this).parent().children('ul').children('li')
                    if ($('li.child>a').hasClass('active')) {
                        $('li.child>a.active').parent().addClass('active');
                        $("ul.level2 li ul").hide();
                        $('li.child>a.active').parent('li').children('ul').show();
                    } else {
                        $("ul.level2 li ul").hide();
                    }
                }
            }
        });
    });

    // checkbox
    $(":checkbox").click(function () {
        var id = $(this).attr('id');
        var flow_id = $("#flow_id").val();
        $.ajax({
            url: `/user-points/?id=` + id + `&flow_id=` + flow_id,
            type: 'GET',
            success: function (data) {
                if (data.status) {
                    // location.reload();
                } else {
                
                }
            }
        });
    });

    /* var scroll = document.querySelector(".line1.active");
    window.addEventListener('scroll', function () {
        var value = window.screenY;
        scroll.style.transform = `scaleY(${value})`;
    }) */
});

$(window).resize(function () {
    var header = $('.navbar-header-fixed').height();
    var ol = $("ol").height();
    var finalHeight = header + 40 + ol + 15;
    // 40 = 20 top , 20 bottom padding
    // 15 breadcurms bottom padding

    /* if (window.matchMedia('(min-width: 1024px)').matches) {
        $('#myScrollspy').affix({ offset: { top: 150 } });
    } else {
        $('#myScrollspy').affix({ offset: { top: 250 } });
    } */


    $("#myScrollspy").css("top", finalHeight);
    // $("ul.flex-row li:first a").addClass("active");
    $("ul.flex-row li a.active").parent().find('ul').show();
    $('a[href^="#"]').bind('click', function (e) {
        e.preventDefault(); // prevent hard jump, the default behavior

        var target = $(this).attr("href"); // Set the target as variable
        // perform animated scrolling by getting top-position of target-element and set it as scroll target
        $('html, body').animate({
            scrollTop: $(target).offset().top - finalHeight
        }, 100);
        return false;
    });
    jQuery(window).on('scroll', function (e) {
        $('li.parent>a').each(function () {
            if ($(this).hasClass('active')) {
                $("ul.flex-row li ul").hide();
                $(this).addClass('active');
                $(this).parent().find('ul').show();
                if ($(this).parent().children('ul')) {
                    $("li.parent > ul").hide();
                    $("li.child > ul").hide();
                    $(this).parent().children('ul').css('display', 'block');
                    $(this).parent().children('ul').children('li')
                    if ($('li.child>a').hasClass('active')) {
                        $('li.child>a.active').parent().addClass('active');
                        $("ul.level2 li ul").hide();
                        $('li.child>a.active').parent('li').children('ul').show();
                    } else {
                        $("ul.level2 li ul").hide();
                    }
                }
            }
        });
    });
}).trigger('resize');

$(window).ready(function () {
    var header = $('.navbar-header-fixed').height();
    var ol = $("ol").height();
    var finalHeight = header + 40 + ol + 15;
    // 40 = 20 top , 20 bottom padding
    // 15 breadcurms bottom padding
    $("#myScrollspy").css("top", finalHeight);
    // $("ul.flex-row li:first a").addClass("active");
    $("ul.flex-row li a.active").parent().find('ul').show();
    $('a[href^="#"]').bind('click', function (e) {
        e.preventDefault(); // prevent hard jump, the default behavior

        var target = $(this).attr("href"); // Set the target as variable
        // perform animated scrolling by getting top-position of target-element and set it as scroll target
        $('html, body').animate({
            scrollTop: $(target).offset().top - finalHeight
        }, 100);
        return false;
    });
    $('li.parent>a').each(function () {
        if ($(this).hasClass('active')) {
            $("ul.flex-row li ul").hide();
            $(this).addClass('active');
            $(this).parent().find('ul').show();
            if ($(this).parent().children('ul')) {
                $("li.parent > ul").hide();
                $("li.child > ul").hide();
                $(this).parent().children('ul').css('display', 'block');
                $(this).parent().children('ul').children('li')
                if ($('li.child>a').hasClass('active')) {
                    $('li.child>a.active').parent().addClass('active');
                    $("ul.level2 li ul").hide();
                    $('li.child>a.active').parent('li').children('ul').show();
                } else {
                    $("ul.level2 li ul").hide();
                }
            }
        }
    });
});