$(document).ready(function () {
    var id = 2;
    var redirectLink = '';
    var deleteLink = '';
    var url = window.location.href;
    var arr = url.split("/");
    var link = arr[arr.length - 1];
    if (link == 'create') {
        redirectLink = '../upload';
        deleteLink = '../delete';
    }
    if (link == 'edit') {
        redirectLink = '../../upload';
        deleteLink = '../../delete';
    }
    var result = arr[0] + "//" + arr[2] + "/";
    // console.log("result :: ", result);
    new FroalaEditor('#content', {
        // Set the file upload URL.
        imageUploadURL: redirectLink,
        imageUploadParams: {
            id: 'my_editor',
            media_key: $("#media_key").val(),
        },
        videoUploadURL: redirectLink,
        videoUploadParams: {
            id: 'my_editor',
            media_key: $("#media_key").val(),
        },

        imageManagerDeleteURL: deleteLink,
        imageManagerDeleteMethod: "POST",
        events: {
            // Catch image removal from the editor.
            'image.removed': function ($img) {
                $.ajax({
                    // Request method.
                    method: "POST",

                    // Request URL.
                    url: deleteLink,

                    // Request params.
                    data: {
                        src: $img.attr('src')
                    }
                })
                    .done(function (data) {
                    })
                    .fail(function (err) {
                        console.log('image delete problem: ' + JSON.stringify(err));
                    })
            },
            // Catch file removal from the editor.
            'video.removed': function ($video) {
                // Do something here.
                // this is the editor instance.
                $.ajax({
                    // Request method.
                    method: "POST",
                    // Request URL.
                    url: deleteLink,

                    // Request params.
                    data: {
                        src: $video.attr('src')
                    }
                })
                    .done(function (data) {
                    })
                    .fail(function (err) {
                    })
            }
        }
    });

    new FroalaEditor('#content1', {
        // Set the file upload URL.
        imageUploadURL: redirectLink,
        imageUploadParams: {
            id: 'my_editor',
            media_key: $("#media_key").val(),
        },
        videoUploadURL: redirectLink,
        videoUploadParams: {
            id: 'my_editor',
            media_key: $("#media_key").val(),
        },

        imageManagerDeleteURL: deleteLink,
        imageManagerDeleteMethod: "POST",
        events: {
            // Catch image removal from the editor.
            'image.removed': function ($img) {
                $.ajax({
                    // Request method.
                    method: "POST",

                    // Request URL.
                    url: deleteLink,

                    // Request params.
                    data: {
                        src: $img.attr('src')
                    }
                })
                    .done(function (data) {
                        console.log('image was deleted');
                    })
                    .fail(function (err) {
                        console.log('image delete problem: ' + JSON.stringify(err));
                    })
            },
            // Catch file removal from the editor.
            'video.removed': function ($video) {
                // Do something here.
                // this is the editor instance.
                console.log(this);
                $.ajax({
                    // Request method.
                    method: "POST",
                    // Request URL.
                    url: deleteLink,

                    // Request params.
                    data: {
                        src: $video.attr('src')
                    }
                })
                    .done(function (data) {
                        console.log('file was deleted');
                    })
                    .fail(function (err) {
                        console.log('file delete problem: ' + JSON.stringify(err));
                    })
            }
        }
    });
    $(".addSubpoint").on('click', function () {
        addSubPoint();
    });

    function addSubPoint() {
        console.log("redirectLink : ", redirectLink);
        console.log("`#content` + id :: ", `#content` + id);
        new FroalaEditor(`#content` + id, {
            // Set the file upload URL.
            initOnClick: true,
            imageUploadURL: redirectLink,
            imageUploadParams: {
                id: 'my_editor',
                media_key: $("#media_key").val(),
            },
            videoUploadURL: redirectLink,
            videoUploadParams: {
                id: 'my_editor',
                media_key: $("#media_key").val(),
            },

            imageManagerDeleteURL: deleteLink,
            imageManagerDeleteMethod: "POST",
            events: {
                // Catch image removal from the editor.
                'image.removed': function ($img) {
                    $.ajax({
                        // Request method.
                        method: "POST",

                        // Request URL.
                        url: deleteLink,

                        // Request params.
                        data: {
                            src: $img.attr('src')
                        }
                    })
                        .done(function (data) {
                            console.log('image was deleted');
                        })
                        .fail(function (err) {
                            console.log('image delete problem: ' + JSON.stringify(err));
                        })
                },
                // Catch file removal from the editor.
                'video.removed': function ($video) {
                    // Do something here.
                    // this is the editor instance.
                    console.log(this);
                    $.ajax({
                        // Request method.
                        method: "POST",
                        // Request URL.
                        url: deleteLink,

                        // Request params.
                        data: {
                            src: $video.attr('src')
                        }
                    })
                        .done(function (data) {
                            console.log('file was deleted');
                        })
                        .fail(function (err) {
                            console.log('file delete problem: ' + JSON.stringify(err));
                        })
                }
            }
        });
        console.log("inside the addsub point ------> ", id);
        var html = `<hr/>
            <div id="point`+ id + `">
            </div>
            <div class="form-row">
                <div class="form-group col-md-4">
                    <label for="title`+ id + `">Sub Point Title</label>
                    <input type="text" class="form-control" id="title`+ id + `" name="title` + id + `">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-12">
                    <label for="content`+ id + `">Sub Point Content</label>
                    <textarea id="content`+ id + `" name="content` + id + `" class="form-control content` + id + `
        maxlength = "800" >
                    </textarea >
                </div >
            </div >
            `;
        $(".sub_points").append(html);
        id++;
    }

    $('#points_form').on('submit', function(e) {
        var title = $('#title').val();
        var title_length = $.trim(title).length;
        var content = $(document).find('.fr-wrapper .fr-element').text();
        var content_length = $.trim(content).length;

        if(title_length || content_length) {
            return true;
        }

        alert('Title and Content not entered');
        return false;
    })
});