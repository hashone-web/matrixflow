var mongoose = require("mongoose");
var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);
var dotenv = require('dotenv').config({
    path: __dirname + '/../.env'
})

const autoIncrement = require('mongoose-auto-increment-fix');

const permissionData = [
    // user permission
    {
        name: 'Create User',
    },
    {
        name: 'View User',
    },
    {
        name: 'Update User',
    },
    {
        name: 'Delete User',
    }
];

const roleData = [
    {
        name: 'Moderator',
        permissions: ['Create User', 'View User', 'Update User', 'Delete User']
    },
    {
        name: 'Admin',
        permissions: ['Create User', 'View User', 'Update User', 'Delete User']
    }
];

const userData = [
    {
        name: 'Jhalak Javiya',
        email: 'jhalakjaviya@gmail.com',
        password: 'zncjK9Qax3CMFVtt',
        mobile_no: '8866553779',
        status: 'active',
        content_password: '123456',
        roleType: 'Admin',
    },
    {
        name: 'Kapeel Patel',
        email: 'matrixmob@gmail.com',
        password: 'KzCL3cgU8p78Kvxz',
        mobile_no: '8000880004',
        status: 'active',
        content_password: '123456',
        roleType: 'Admin',
    },
    {
        name: 'Amisha Nena',
        email: 'amisha.n@matrixmob.com',
        password: 'sqATfj95UpvKZDXZ',
        mobile_no: '8128917008',
        status: 'active',
        content_password: '123456',
        roleType: 'Moderator'
    },
    {
        name: 'Ishita Bhadeshiya',
        email: 'ishita.b@matrixmob.com',
        password: 'zjcAJxcR5Rgaftcj',
        mobile_no: '8511086752',
        status: 'active',
        content_password: '123456',
        roleType: 'Moderator'
    }
];


mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
}, (err, client) => {
    if (err) {
        console.error(err)
        return
    } else {
        console.log("DB CONNECTED!");

        autoIncrement.initialize(mongoose.connection);

        var Permission = require('../model/permissons');
        var Role = require('../model/roles');
        var User = require('../model/users');

        var permissonSeed = async () => {
            for (var index in permissionData) {
                var permission = new Permission(permissionData[index]);
                await permission.save();
            }
            console.log("PERMISSIONS SUCCESSFULLY CREATED!");
            return permission;
        }

        var roleSeed = async () => {
            for (var index in roleData) {
                var permission_ids = [];
                var permissions = roleData[index].permissions;
                for (let permission in permissions) {
                    var res = await Permission.findOne({ name: permissions[permission] }).select("_id");
                    permission_ids.push(res._id);
                }
                roleData[index].permissions = permission_ids
                var role = new Role(roleData[index]);
                await role.save();
            }
            console.log("ROLES SUCCESSFULLY CREATED!");
            return role;
        }

        var userSeed = async () => {
            for (var index in userData) {
                userData[index].password = bcrypt.hashSync(userData[index].password, salt);
                var res = await Role.findOne({ name: userData[index].roleType }).select("_id");
                userData[index].role = res;
                delete userData[index].roleType;
                var users = new User(userData[index]);
                await users.save();
            }
            console.log("USERS SUCCESSFULLY CREATED!");
            return users;
        }

        var MyFunction = async () => {
            await permissonSeed()
                .then(async permission => {
                    await roleSeed()
                        .then(async role => {
                            await userSeed()
                                .then(async user => {
                                    process.exit(1);
                                })
                                .catch(error => {
                                    console.log("user error : ", error);
                                })
                        })
                        .catch(error => {
                            console.log("role error : ", error)
                        })
                })
                .catch(error => {
                    console.log("permision error : ", error)
                })
        }

        MyFunction();
    }
});