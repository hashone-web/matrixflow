$(document).ready(function () {
    $("#reset").click(function () {
        var password = $("#password").val();
        // console.log("Password : ", password);
        var reenter_password = $("#reenter_password").val();
        // console.log("reenter_password : ", reenter_password);
        if (password !== reenter_password) {
            $("#reenter_password").addClass('is-invalid');
            return false;
        } else {
            return true;
        }
    })
});
