var UserHelper = require('../helpers/user');
var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);
var User = require('../model/users');
var UserPoint = require('../model/user_points');
var UserFlow = require('../model/user_flow');
var ImageHelper = require('../helpers/image_upload');
var RoleHelper = require('../helpers/role');
var mongoose = require("mongoose");
var moment = require('moment');
var fs = require("fs");

module.exports = {
    login: async (email, pswd) => {
        try {
            var UserLogin = await UserHelper.findOneUser({ 'email': email, 'deleted_at': null });
            if (UserLogin.status == 1) {
                // check status is not pending
                var UserStatus = await UserHelper.findOneUser({ 'email': email, 'status': { $eq: 'active' }, 'deleted_at': null });
                if (UserStatus.status == 0) {
                    return obj = {
                        email: UserStatus.email,
                        error: 'You are not allowed to login'
                    }
                } else {
                    if (bcrypt.compareSync(pswd, UserStatus.password)) {
                        return UserStatus;
                    } else {
                        return obj = {
                            email: UserStatus.email,
                            error: 'Incorrect Password'
                        }
                    }
                }
            } else {
                // UserLogin with message;
                UserLogin.error = 'User not found';
                return UserLogin;
            }
        } catch (err) {
            return err;
        }
    },

    getUser: async (req, res, next) => {
        try {
            var Helper = await UserHelper.findOnePopulate({ _id: req.params.id });
            if (Helper) {
                res.render('logedin_index', {
                    what: 'pages/users/profile',
                    title: 'User',
                    user: Helper,
                    page_name: 'users',
                })
            } else {
                res.redirect('/users');
            }
        } catch (error) {
            console.log("getUser Error :: ", error);
            res.redirect('/');
        }
    },

    create: async (req, res, next) => {
        try {
            if (req.user.role.name === 'Admin') {
                req.active = req.path.split('/')[1];
                if (req.session.permissions.findIndex(permission => permission === "Create User") === -1) {
                    res.render('logedin_index', {
                        what: 'pages/403',
                        title: 'Error',
                        message: 'You Have No Rights To Create User',
                        href: req.baseUrl,
                        user: req.user,
                        csrftoken: req.token,
                        page_name: 'users'
                    });
                } else {
                    res.render('logedin_index', {
                        what: 'pages/users/create',
                        title: 'Create User',
                        csrftoken: req.token,
                        user: req.user,
                        user_data: '',
                        link: req.active,
                        error: '',
                        created_by: req.user._id,
                        status: User.schema.path('status').enumValues,
                        roles: await RoleHelper.getAllRolesName(),
                        page_name: 'users'
                    });
                }
            } else {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To Create User',
                    href: req.baseUrl,
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'users',
                });
            }
        } catch (error) {
            console.log("create user error :: ", error);
            res.redirect('/');
        }
    },

    view: async (req, res, next) => {
        try {
            if (req.user.role.name === 'Admin') {
                var permission = {};
                var perPage = req.query.limit ? JSON.parse(req.query.limit) : 5;
                var page = req.query.page ? req.query.page : 1;
                var sort_by = req.query.sort_by ? req.query.sort_by : '_id';
                var sort_type = req.query.sort_type ? req.query.sort_type : 'desc';
                var admin_id;
                var isAdmin = false;
                var query = {};
                query['deleted_at'] = null;
                // for search query 
                if (req.query.search_field) {
                    query[req.query.search_field] = new RegExp(req.query.search_string, "ig");
                }

                // for filter
                if (req.query.filter_status) {
                    query['status'] = req.query.filter_status;
                    filter_status = req.query.filter_status;
                }
                var role = await commonFindOneData('Role', { name: 'Admin', deleted_at: null });
                if (!role.errorMessage) {
                    admin_id = role._id;
                }
                if (JSON.stringify(req.user.role._id) == JSON.stringify(role._id)) {
                    isAdmin = true;
                }
                if (req.session.permissions.findIndex(permission => permission === "View User") === -1) {
                    res.render('logedin_index', {
                        what: 'pages/403',
                        title: 'Error',
                        message: 'You Have No Rights To View User List',
                        href: '/',
                        user: req.user,
                        csrftoken: req.token,
                        page_name: 'users'
                    });
                } else {
                    var deletePermission = req.session.permissions.findIndex(permission => permission === "Delete User");
                    if (deletePermission !== -1) {
                        permission.deleteUser = true
                    }
                    var createPermission = req.session.permissions.findIndex(permission => permission === "Create User");
                    if (createPermission !== -1) {
                        permission.createUser = true
                    }
                    var editPermission = req.session.permissions.findIndex(permission => permission === "Update User");
                    if (editPermission !== -1) {
                        permission.editUser = true
                    }
                    await User.find(query).populate('role')
                        .sort([[sort_by, sort_type]])
                        .skip((perPage * page) - perPage)
                        .limit(perPage)
                        .exec((err, users) => {
                            User.countDocuments(query).exec(function (err, count) {
                                if (err) return next(err);
                                else {
                                    res.render('logedin_index', {
                                        what: 'pages/users/index',
                                        title: 'Users',
                                        page_name: 'users',
                                        user: req.user,
                                        userList: users,
                                        current: page,
                                        sort_by: sort_by,
                                        sort_type: sort_type,
                                        pages: Math.ceil(count / perPage),
                                        status: User.schema.path('status').enumValues,
                                        success: req.flash('success'),
                                        filter_status: req.query.filter_status,
                                        search_field: req.query.search_field,
                                        search_string: req.query.search_string,
                                        admin_id: admin_id,
                                        isAdmin: isAdmin,
                                        isPermisson: permission,
                                        csrftoken: req.token
                                    });
                                }
                            })
                        })
                }
            } else {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To View Users',
                    href: req.baseUrl,
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'users',
                });
            }
        } catch (error) {
            console.log("paging error : ", error);
            res.redirect('/');
        }
    },

    store: async (req, res, next) => {
        try {
            if (req.body._csrf == undefined || req.body._csrf == null) {
                res.render('index', {
                    what: 'error',
                    title: 'ERROR',
                    message: "CSRF Mismatch",
                    error: {
                        status: 400,
                        stack: 'CSRF'
                    }
                });
            } else {
                req.active = req.path;
                var obj = {
                    name: req.body.name,
                    email: req.body.email,
                    mobile_no: req.body.mobile_no,
                    role: req.body.role
                };
                if (req.body.status) {
                    obj.status = req.body.status
                }
                var emailDuplication = await UserHelper.findOneUser({ 'email': req.body.email });
                if (emailDuplication.status == 1) { // duplicate email
                    res.render('logedin_index', {
                        what: 'pages/users/create',
                        title: 'Create User',
                        user: req.user,
                        user_data: obj,
                        link: 'create',
                        error: 'Email already exist.',
                        created_by: req.user._id,
                        status: User.schema.path('status').enumValues,
                        roles: await RoleHelper.getAllRolesName(),
                        csrftoken: req.body._csrf,
                        page_name: 'users'
                    });
                } else {
                    req.body.password = bcrypt.hashSync(req.body.password, salt);
                    var user = new User(req.body);
                    user.save()
                        .then(async user => {
                            if (user) {
                                if (req.files) {
                                    var fileName = await ImageHelper.userFile(req.body.picture, user._id);
                                    var Helper = await UserHelper.findAndUpdate({ _id: user._id }, { profile_picture: fileName });
                                    if (Helper) {
                                        req.flash(
                                            'success',
                                            `User Successfully Created`
                                        );
                                        res.redirect('/users')
                                    } else {
                                        res.redirect('/');
                                    }
                                } else {
                                    res.redirect('/users')
                                }
                            } else {
                                console.log("createUser : Something went wrong");
                            }
                        })
                        .catch(error => {
                            console.log("store Error :: ", error);
                            res.redirect('/');
                        })
                }
            }
        } catch (error) {
            console.log("store user error : ", error);
            res.redirect('/');
        }
    },

    edit: async (req, res, next) => {
        try {
            var referer = req.headers.referer.split('users/')[1];
            if (req.session.permissions.findIndex(permission => permission === "Update User") === -1 && req.params.id.toString() !== req.user._id.toString() && req.user.role.name !== 'Admin') {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To Edit User',
                    href: '/users',
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'users',
                });
            } else {
                var flow;
                var userFlow;
                var uFlow;
                var role = await commonFindOneData('Role', { name: 'Admin', deleted_at: null });
                req.active = req.path.split('/')[2];
                req.session['redirect'] = referer;
                User.findById(req.params.id)
                    .then(async user => {
                        if (user.role.toString() != role._id.toString()) {
                            flow = await commonFindData('Flow', { deleted_at: null });
                        } else {
                            flow = '';
                        }
                        user = user.toJSON({ virtuals: true });
                        res.render('logedin_index', {
                            what: 'pages/users/create',
                            title: 'Edit User',
                            user: req.user,
                            user_data: user,
                            link: req.active,
                            flow: flow,
                            userFlows: uFlow,
                            error: '',
                            csrftoken: req.token,
                            created_by: '',
                            status: User.schema.path('status').enumValues,
                            roles: await RoleHelper.getAllRolesName(),
                            page_name: 'users',
                        });
                    })
                    .catch(err => {
                        console.log("edit error :: ", err);
                        res.redirect('/');
                    })
            }

        } catch (error) {
            console.log("edit error :: ", error);
            res.redirect('/');
        }
    },

    update: async (req, res, next) => {
        try {
            if (req.body._csrf == undefined || req.body._csrf == null) {
                res.render('index', {
                    what: 'error',
                    title: 'ERROR',
                    message: "CSRF Mismatch",
                    error: {
                        status: 400,
                        stack: 'CSRF'
                    }
                });
            } else {
                var id = req.params.id;
                var flow_details = [];
                var obj = {
                    name: req.body.name,
                    mobile_no: req.body.mobile_no,
                    updated_at: Date.now(),
                }
                if (req.body.role) {
                    obj['role'] = req.body.role
                }
                var query = { 'email': req.body.email, '_id': { $ne: id } };
                var emailDuplication = await UserHelper.findOneUser(query);
                if (emailDuplication.status == 0) { // 0 = no email found in db
                    var Helper = await UserHelper.findOneUser({ _id: id });
                    if (req.body.picture) {
                        var Helper = await UserHelper.findOneUser({ _id: id });
                        if (Helper.profile_picture) {
                            await fs.unlink('public/storage/users/' + id + '/image/' + Helper.profile_picture, function (err) {
                                if (err) {
                                    console.log("ERROR UNLINK : ", err)
                                    return err;
                                }
                            });
                        }
                        obj.profile_picture = await ImageHelper.userFile(req.body.picture, id);
                    };
                    if (req.body.content_password) {
                        obj.content_password = req.body.content_password;
                    };
                    if (req.body.content_password) {
                        obj.content_password = req.body.content_password;
                    };
                    if (req.body.status) {
                        obj.status = req.body.status;
                    };
                    obj.updated_by = req.user._id;
                    var Helper = await UserHelper.findAndUpdate({ _id: id }, obj);
                    if (req.body.flow == undefined || req.body.flow == null) {
                        var user_flow = await UserFlow.findOne({ user_id: id });
                        if (user_flow) {
                            if (user_flow.flow_details.length > 0) {
                                for (let i = 0; i < user_flow.flow_details.length; i++) {
                                    user_flow.flow_details[i].flow_id = user_flow.flow_details[i].flow_id;
                                    user_flow.flow_details[i].status = 'uncompleted';
                                    var user_points = await UserPoint.updateMany({ flow_id: user_flow.flow_details[i].flow_id }, { status: 'uncompleted', updated_at: Date.now() }, { new: true });
                                }
                                var user_flow = await UserFlow.findOneAndUpdate({ user_id: id, deleted_at: null }, { flow_details: [] }, { upsert: true, new: true });
                            }
                        }
                    } else {
                        if (typeof req.body.flow == "object") {
                            var flows = await UserFlow.findOne({ user_id: id, deleted_at: null }, { flow_details: 1 });
                            var details = flows.flow_details;
                            for (let i = 0; i < req.body.flow.length; i++) {
                                var index = details.findIndex(x => x.flow_id == req.body.flow[i]);
                                if (index == -1) {
                                    flow_details.push({ 'flow_id': req.body.flow[i], status: 'uncompleted' })
                                } else {
                                    flow_details.push(details[index]);
                                }
                            }
                        } else {
                            flow_details.push({ 'flow_id': req.body.flow, status: 'uncompleted' });
                        }
                        var user_flow = await UserFlow.findOneAndUpdate({ user_id: id, deleted_at: null }, { flow_details: flow_details }, { upsert: true, new: true });
                    }
                    if (Helper) {
                        req.flash(
                            'success',
                            `User Successfully Updated`
                        );
                        if (req.user.role.name == 'Admin') {
                            res.redirect('/users/' + req.session.redirect);
                        } else {
                            res.redirect('/flows');
                        }
                    } else {
                        req.flash(
                            'info',
                            `Something Went User Updated`
                        );
                        res.redirect('/users');
                    }
                } else {
                    res.render('logedin_index', {
                        what: 'pages/users/create',
                        title: 'EDIT USER',
                        user: req.user,
                        link: 'edit',
                        user_data: obj,
                        error: "Email Already Exist.",
                        csrftoken: req.body._csrf,
                        status: User.schema.path('status').enumValues,
                        roles: await RoleHelper.getAllRolesName(),
                        page_name: 'users'
                    });
                }
            }
        } catch (error) {
            console.log("update error :: ", error);
            res.redirect('/');
        }
    },

    destroy: (req, res, next) => {
        try {
            if (req.body._csrf == undefined || req.body._csrf == null) {
                res.render('index', {
                    what: 'error',
                    title: 'ERROR',
                    message: "CSRF Mismatch",
                    error: {
                        status: 400,
                        stack: 'CSRF'
                    }
                });
            } else {
                var id = req.params.id;
                var obj = {
                    deleted_at: Date.now(),
                }
                // res.send("test");
                User.findByIdAndUpdate(id, obj, { new: true })
                    .then(user => {
                        req.flash(
                            'success',
                            `User Successfully Destory!`
                        );
                        res.send(Helper);
                    })
                    .catch(error => {
                        console.log("destroy error :: ", error);
                        res.redirect('/');
                    })
            }
        } catch (error) {
            console.log("destroy error :: ", error);
            res.redirect('/');
        }
    },

    password: (req, res, next) => {
        try {
            res.render('logedin_index', {
                what: 'auth/reset_password',
                user: req.user,
                title: 'Reset User Passwoad',
                csrftoken: req.token,
                page_name: 'users'
            });
        } catch (error) {
            res.render('/');
        }
    },

    reset_password: (req, res, next) => {
        try {
            if (req.body._csrf == undefined || req.body._csrf == null) {
                res.render('index', {
                    what: 'error',
                    title: 'Rest Password',
                    message: "CSRF Mismatch",
                    error: {
                        status: 400,
                        stack: 'CSRF'
                    }
                });
            } else {
                req.body.password = bcrypt.hashSync(req.body.password, salt);
                User.findByIdAndUpdate({ _id: req.body.id }, { password: req.body.password, updated_at: Date.now() }, { new: true })
                    .then(user => {
                        req.flash(
                            'success',
                            `User Password Change!`
                        );
                        res.redirect('/users')
                    })
                    .catch(error => {
                        console.log("reset_password error :: ", error);
                        res.redirect('/');
                    })
            }
        } catch (error) {
            console.log("reset_password error :: ", error);
        }
    },

    status: (req, res, next) => {
        try {
            if (req.body._csrf == undefined || req.body._csrf == null) {
                res.render('index', {
                    what: 'error',
                    title: 'ERROR',
                    message: "CSRF Mismatch",
                    error: {
                        status: 400,
                        stack: 'CSRF'
                    }
                });
            } else {

                User.findByIdAndUpdate(req.body.id, {
                    status: req.body.status,
                    updated_at: Date.now()
                }, {
                    new: false
                })
                    .then(article => {
                        req.flash(
                            'success',
                            `User Status Change Successfully`
                        );
                        res.json({
                            status: true,
                        });
                    })
                    .catch(error => {
                        res.json({
                            status: false,
                        });
                    })
            }
        } catch (error) {
            res.json({
                status: false,
            });
        }
    },
}