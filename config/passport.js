var passport = require('passport')
    , LocalStrategy = require('passport-local').Strategy
var UserController = require('../controller/UserController');
var User = require('../model/users');

passport.serializeUser(function (user, done) {
    done(null, user.id);
});

passport.deserializeUser(function (id, done) {
    User.findById(id, function (err, user) {
        done(err, user);
    });
});

passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    passwordField: 'password'
}, (email, password, done) => {
    UserController.login(email, password)
        .then(response => {
            return done(null, response);
        })
        .catch(err => {
            console.log("local-login error : ", err)
            return done(err, { message: "Something went wrong" });
        });
}));

module.exports = passport;