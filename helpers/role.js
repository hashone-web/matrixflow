var Role = require("../model/roles");

module.exports = {
    getAllRole: () => {
        return new Promise((resolve, reject) => {
            Role.find({ deleted_at: null })
                .populate('permissions')
                .exec(function (err, role) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(role);
                    }
                });
        })
    },

    getRoleById: (query) => {
        return new Promise((resolve, reject) => {
            Role.find(query)
                .populate('permissions')
                .exec(function (err, role) {
                    if (err) {
                        reject(err);
                    } else {
                        resolve(role);
                    }
                });
        })
    },

    findAndUpdate: (con, query) => {
        return new Promise((resolve, reject) => {
            Role.findOneAndUpdate(con, query, { new: true })
                .then(role => {
                    resolve(role);
                })
                .catch(err => {
                    reject(err);
                })
        })
    },

    getAllRolesName: () => {
        return new Promise((resolve, reject) => {
            Role.find({ deleted_at: null }).select({ "name": 1})
                .then(role => {
                    resolve(role);
                })
                .catch(err => {
                    reject(err);
                })
        })
    },
}