var path = require("path");
var fs = require("fs");
const mkdirp = require('mkdirp');
// Gets a filename extension.
const make_random = require('./helpers/make_random');

module.exports = {
    upload: (req, callback) => {
        var arr = req.files.file.name.split('.');
        var newName = make_random.random(12) + `.` + arr[arr.length - 1];
        dir = path.join(appRoot, 'public/storage/points/' + req.body.media_key);
        if (!fs.existsSync(dir)) {
            mkdirp.sync(dir, { recursive: true });
        }
        req.files.file.mv(dir + `/` + newName);
        var obj = { link: '/points/' + req.body.media_key + '/' + newName };
        callback(null, obj);
    },
    
    _delete: (src, callback) => {
        src = 'public/storage' + src
        fs.unlink(path.join(appRoot, src), function (err) {
            if (err) {
                console.log("fs unlink err :: ", err);
                return callback(err);
            }
            return callback(null, true);
        });
    }
}

