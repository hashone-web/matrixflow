const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Role = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    permissions: [
        {
            type: Schema.Types.ObjectId,
            ref: 'Permisson',
        },
    ],
    created_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    deleted_at: {
        type: Date,
        default: null,
    }
}, {
    collection: 'roles'
});

module.exports = mongoose.model('Role', Role);