var express = require('express');
var router = express.Router();
var PermissionController = require('../controller/PermissionController');
// var auth = require('../config/auth');

// permissions/
router.get('/', PermissionController.view);

// permissions/create
router.post('/create', PermissionController.create);

// permissions/:id/edit
router.get('/:id/edit', PermissionController.edit);

// permissions/:id
router.post('/:id', PermissionController.update);

// permissions/:id/destroy
router.post('/:id/destroy', PermissionController.destroy);

module.exports = router;
