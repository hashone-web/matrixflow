var express = require('express');
var router = express.Router();
var RoleController = require('../controller/RoleController');

// roles/
router.get('/', RoleController.view);

// roles/create
router.post('/create', RoleController.create);

// roles/:id/edit
router.get('/:id/edit', RoleController.edit);

// roles/:id
router.post('/:id', RoleController.update);

// roles/:id/destory
router.post('/:id/destroy', RoleController.destroy);

module.exports = router;
