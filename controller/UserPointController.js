var UserHelper = require('../helpers/user');
var UserPoints = require('../model/user_points');
var UserFlow = require('../model/user_flow');
var Point = require('../model/point');
var mongoose = require("mongoose");
var fs = require("fs");

module.exports = {
    update: async (req, res, next) => {
        try {
            var id = req.query.id;
            var flow_id = req.query.flow_id;
            var completed = []
            var sortable_points = await findPoints(flow_id);
            var isExist = await commonFindOneData('UserPoint', { point_id: id, user_id: req.user._id });
            // console.log("isExist :: ", isExist);
            if (isExist.errorMessage) {
                // console.log("inside thhe create & save user points -------------> ");
                var obj = {
                    point_id: id,
                    flow_id: flow_id,
                    status: 'completed',
                    user_id: req.user._id
                }
                var user_points = new UserPoints(obj);
                await user_points.save();
            } else {
                var status = (isExist.status == 'completed') ? 'uncompleted' : 'completed';
                var update = await commonFindOneAndUpdateData('UserPoint', { _id: isExist._id }, { status: status, updated_at: Date.now() });
                // console.log("inside the else case ----->");
            }

            for (let i = 0; i < sortable_points.length; i++) {
                // console.log("inside the for loop *****************");
                // get the all completed User Points 
                var completedPoints = await UserPoints.findOne({ status: { $ne: 'uncompleted' }, point_id: sortable_points[i]._id, flow_id: sortable_points[i].flow, user_id: req.user._id });
                // console.log("completedPoints :query result : ", completedPoints);
                if (completedPoints != null) {
                    completed.push(completedPoints);
                }
                // console.log("completed :: ", completed);
            }
            // console.log("completed :: ", completed.length);
            // console.log("sortable_points :: ", sortable_points.length);
            var updateObject = {
                user_id: req.user._id,
                flow_id: flow_id,
            }
            if (sortable_points.length == completed.length) {
                updateObject.status = 'completed'
                await updateUserFlow(updateObject);
            } else {
                updateObject.status = 'uncompleted'
                await updateUserFlow(updateObject);
            }

            res.json({ status: true })
        } catch (error) {
            console.log("update error :: ", error);
            res.json({ status: false })
        }
    }
}

function findPoints(flow_id) {
    return new Promise((resolve, rejects) => {
        var points = [];
        var sortable_points = [];
        var user_points = UserPoints.find({ deleted_at: null }, { point_id: 1, status: 1 });
        // console.log("user_points : :", user_points);
        for (let i = 0; i < user_points.length; i++) {
            if (user_points[i].status == 'completed') {
                points.push(user_points[i].point_id);
            }
        }

        Point.find({ flow: flow_id, deleted_at: null })
            .sort([['version', 'asc']])
            .lean()
            .then(async result => {
                for (let i = 0; i < result.length; i++) {
                    var version = result[i].version.split(".");
                    result[i].children = new Array;
                    if (version[1] == 0) {
                        // console.log("result[i]._id ::", result[i])
                        var index = points.findIndex(x => x.toString() == result[i]._id.toString());
                        if (index >= 0) {
                            result[i].checked = true;
                        }
                        sortable_points.push(result[i])
                    } else {
                        var index = sortable_points.findIndex(inx => inx.version == version[0] + `.0`)
                        if (index >= 0) {
                            sortable_points[index].children.push(result[i]);
                        }
                    }
                }
                resolve(sortable_points);
            })
            .catch(error => {
                console.log("points find error :: ", error);
                rejects(error);
            })
    })
}

async function updateUserFlow(data) {
    try {
        // console.log("data :: ", data);
        var UserFlow = await commonFindOneData('UserFlow', { user_id: data.user_id, deleted_at: null });
        // console.log("UserFlow :: ", UserFlow);
        var index = await UserFlow.flow_details.findIndex(x => x.flow_id == data.flow_id)
        // console.log("index ::", index);
        // console.log("Userflow index ele :: ", UserFlow.flow_details[index].flow_id);
        if (index >= 0) {
            // console.log("data.status :: ", data.status)
            UserFlow.flow_details[index].status = data.status
            // console.log("UserFlow.flow_details :::::: ", UserFlow.flow_details);
        }
        var userFlowUpdate = await commonFindOneAndUpdateData('UserFlow', { _id: UserFlow._id }, { flow_details: UserFlow.flow_details });
        // console.log("userFlowUpdate :: ", userFlowUpdate);
        return true;
    }
    catch (error) {
        return false;
    }
}