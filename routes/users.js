var express = require('express');
var router = express.Router();
var UserController = require('../controller/UserController');
var auth = require('../config/auth');
var Token = require('../config/token');

// users/
router.get('/', [auth.isLoggedIn, auth.csrfConfig], UserController.view);

// users/create
router.get('/create', [auth.isLoggedIn, auth.csrfConfig], UserController.create);

// users/
router.post('/', [auth.isLoggedIn], UserController.store);

// users/password
router.get('/password', [auth.isLoggedIn,  auth.csrfConfig], UserController.password);

// users/reset_password
router.post('/reset-password', [auth.isLoggedIn], UserController.reset_password);

// users/status
router.post('/status', [auth.isLoggedIn], UserController.status);

// users/:id/destory
router.post('/:id/destroy', [auth.isLoggedIn], UserController.destroy);

// users/:id
router.post('/:id', [auth.isLoggedIn], UserController.update);

// users/:id/edit
router.get('/:id/edit', [auth.isLoggedIn, auth.csrfConfig], UserController.edit);

// users/:id
router.get('/:id', [auth.isLoggedIn], UserController.getUser);

module.exports = router;