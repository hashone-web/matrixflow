$(document).ready(function () {
    var message = $(".toastr").val();
    var messageType = $('.toastr').hasClass('success');
    toastrMessage(message, messageType);
});

function toastrMessage(message, messageType) {
    if (message.length > 0) {
        if (messageType == true) {
            toastr.success(message, { timeOut: 1000 });
        }
        if (messageType == false) {
            toastr.error(message, { timeOut: 1000 });
        }
    }
}