const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment-fix');

const Flow = new Schema({
    title : {
        type: String,
        required: true
    },
    slug : {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
    },
    status: {
        type: String,
        enum: ['pending', 'active', 'inactive'],
        default: 'pending'
    },
    sort: {
        type: Number,
        default: 0
    },
    department : {
        type: Number,
        ref: 'Department',
        required: true
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    deleted_at: {
        type: Date,
        default: null,
    }
}, {
    collection: 'flows'
});

Flow.plugin(autoIncrement.plugin, {
    model: 'Flow',
    startAt: 1,
    incrementBy: 1
});

module.exports = mongoose.model('Flow', Flow);