const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Permisson = new Schema({
    name: {
        type: String,
        required: true,
        unique: true
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    deleted_at: {
        type: Date,
        default: null,
    }
}, {
    collection: 'permissons'
});

module.exports = mongoose.model('Permisson', Permisson);