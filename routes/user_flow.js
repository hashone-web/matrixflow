var express = require('express');
var router = express.Router();
var UserController = require('../controller/UserFlowController');
var auth = require('../config/auth');
var Token = require('../config/token');

// users/flow-assign
router.post('/flow-assign',  UserController.flow_assign);

// users/:id/destory
router.post('/destroy', [auth.isLoggedIn], UserController.destroy);

router.get('/:id', [auth.isLoggedIn, auth.csrfConfig], UserController.flow_status);

module.exports = router;