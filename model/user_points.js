const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment-fix');

const UserPoint = new Schema({
    user_id: {
        type: Number,
        ref: 'Flow',
        required: true
    },
    flow_id: {
        type: Number,
        ref: 'Flow',
        required: true
    },
    point_id: {
        type: Number,
        ref: 'Point',
        required: true
    },
    status: {
        type: String,
        enum: ['completed', 'uncompleted'],
        default: 'uncompleted'
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    deleted_at: {
        type: Date,
        default: null,
    }
}, {
    collection: 'user_points'
});

UserPoint.plugin(autoIncrement.plugin, {
    model: 'UserPoint',
    startAt: 1,
    incrementBy: 1
});

module.exports = mongoose.model('UserPoint', UserPoint);