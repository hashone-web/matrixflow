var User = require("../model/users");
var UserHelper = require('../helpers/user');

module.exports = {
    isLoggedIn: async (req, res, next) => {
        if(req.user){
            req.user = await UserHelper.findOnePopulate({ _id: req.user._id });
            if (req.isAuthenticated()) {
                next();
            } else {
                console.log("error while login in auth---------");
                res.redirect('/');
            }
        } else {
            res.redirect('/');
        }
    },

    csrfConfig: (req, res, next) => {
        var token = req.csrfToken();
        if (token) {
            req.token = token;
            next();
        } else {
            res.redirect('public/error', {
                message: "csrf error",
                error: {
                    status: 500,
                    stack: 'CSRF'
                }
            });
        }
    },

    permissionCheck: (req, res, next) => {
        return new Promise((resolve, reject) => {
            var userID = req.passport.user;
            var arrPermission = [];
            User.findOne({ _id: userID })
                .populate({
                    path: 'role',
                    populate: [{
                        path: 'permissions',
                        select: { '_id': 0, 'name': 1 },
                    }],
                })
                .exec(async (err, doc) => {
                    if (doc) {
                        for (var permission in doc.role.permissions) {
                            arrPermission.push(doc.role.permissions[permission].name);
                        }
                        resolve(arrPermission);
                        // next();
                    } else {
                        // res.redirect('/');
                        reject();
                    }
                });
        });
    }
}