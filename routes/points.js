var express = require('express');
var router = express.Router();
var PointController = require('../controller/PointController');
var auth = require('../config/auth');

// point/
/* router.get('/', [auth.isLoggedIn, auth.csrfConfig], PointController.view); */

// point/create
router.get('/create', [auth.isLoggedIn, auth.csrfConfig], PointController.create);

// point/store
router.post('/', [auth.isLoggedIn], PointController.store);

// points/status
router.post('/status', [auth.isLoggedIn], PointController.status);

// point/:id/edit
router.get('/:id/edit', [auth.isLoggedIn, auth.csrfConfig], PointController.edit);

// point/:id
router.post('/:id', [auth.isLoggedIn], PointController.update);

// point/:id/destory
router.post('/:id/destroy', [auth.isLoggedIn], PointController.destroy);

module.exports = router;
