const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment-fix');

const UserFlow = new Schema({
    user_id: {
        unique: true,
        type: Number,
        ref: 'User',
    },
    flow_details: [{
        flow_id: {
            type: Number,
            ref: 'Flow',
        },
        status: {
            type: String,
            enum: ['completed', 'uncompleted'],
            default: 'uncompleted'
        },
    }],
    created_by: {
        type: Number,
        ref: 'User'
    },
    updated_by: {
        type: Number,
        ref: 'User'
    },
    deleted_at: {
        type: Date,
        default: null,
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    collection: 'user_flows'
});

UserFlow.plugin(autoIncrement.plugin, {
    model: 'UserFlow',
    startAt: 1,
    incrementBy: 1
});

module.exports = mongoose.model('UserFlow', UserFlow);