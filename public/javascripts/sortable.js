$(document).ready(function () {
    
    $(".destroy").click(destroy);
    var updateOutput = function (e) {
        var list = e.length ? e : $(e.target),
            output = list.data('output');
        if (window.JSON) {
            output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
        } else {
            output.val('JSON browser support required for this demo.');
        }
    };

    // activate Nestable for list 
    $('#nestable').nestable({
        group: 1,
        maxDepth: 3
    })
        .on('change', updateOutput);

    // output initial serialised data
    updateOutput($('#nestable').data('output', $('#nestable-output')));

});

function destroy() {
    let token = $("#csrf").val();
    // console.log("token --> ", token);
    var par = $(this).parent();
    console.log("par ----------> ", par)
    var id = par.children();
    console.log("id ==> ", id);
    console.log(id.html());
    $.confirm({
        'boxWidth': '500px',
        'useBootstrap': false,
        'title': 'Delete Confirmation',
        'message': 'You are about to delete this item. <br />It cannot be restored at a later time! Continue?',
        'buttons': {
            'Yes': {
                'class': 'blue',
                'action': function () {
                    jQuery.ajax({
                        type: 'POST',
                        url: `/points/` + id.html() + `/destroy`,
                        data: {
                            _csrf: token
                        },
                        success: function (response) {
                            window.location.href = window.location.href
                        },
                    });
                }
            },
            'No': {
                'class': 'gray',
                'action': function () {
                    jQuery.ajax({
                        type: 'GET',
                        url: window.location.href
                    });
                }
            }
        }
    });
};