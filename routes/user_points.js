var express = require('express');
var router = express.Router();
var UserPointController = require('../controller/UserPointController');
var auth = require('../config/auth');

// user-points/
router.get('/', [auth.isLoggedIn], UserPointController.update);

module.exports = router;