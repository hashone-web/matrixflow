var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var dotenv = require('dotenv').config();
const bodyParser = require('body-parser');
var mongoose = require("mongoose");
var passport = require('passport');
var session = require('express-session');
var flash = require('connect-flash');
const csrf = require('csurf');
const moment = require('moment');
const morgan = require('morgan');
const fs = require('fs');
const autoIncrement = require('mongoose-auto-increment-fix');

const fileUpload = require('express-fileupload');
var commonQuery = require('./helpers/commonQuery');
// var winston = require('./helpers/winston');

var path = require('path');
global.appRoot = path.resolve(__dirname);

var app = express();

var csrfProtection = csrf({ cookie: true });

let sessOpts = {
    secret: process.env.SESSION_SECRET || '4wH8VqKa0gpELXK5x3xnIAFRoIw4FHkl',
    resave: true,
    saveUninitialized: false,
    cookie: {
        "secure": process.env.SECURE_SESSION === undefined? false: process.env.SECURE_SESSION === 'true',
        "maxAge": (parseInt(process.env.SESSION_MAX_AGE) * 60 * 60 * 1000) || (6 * 60 * 60 * 1000)
    },
    name: 'SID',
    unset: 'destroy'
};

// view engine setup
app.engine('ejs', require('ejs-locals'));
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(require('ejs-yield'));
if(process.env.DEBUG === 'true') {
    app.use(logger('dev'));
}
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/users', express.static(__dirname + '/public/storage/users'));
app.use('/points', express.static(__dirname + '/public/storage/points'));
app.use('/uploads', express.static(__dirname + '/uploads'));
app.use(cookieParser());
app.use(fileUpload());
// app.use(morgan('combined', { stream: winston.stream }));
mongoose.connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useCreateIndex: true,
    useFindAndModify: false
}, (err, client) => {
    if (err) {
        console.error(err)
        return
    } else {
        console.log("DB CONNECTED!");

        autoIncrement.initialize(mongoose.connection);

        var auth = require("./config/auth");
        var indexRouter = require('./routes/index');
        var usersRouter = require('./routes/users');
        var permissionsRouter = require('./routes/permissions');
        var rolesRouter = require('./routes/roles');
        var departmentsRouter = require('./routes/departments');
        var flowRouter = require('./routes/flows');
        var pointRouter = require('./routes/points');
        var user_flowRouter = require('./routes/user_flow');
        var user_pointsRouter = require('./routes/user_points');

        app.use('/', indexRouter);
        app.use('/users', usersRouter);
        app.use('/permissions', permissionsRouter);
        app.use('/roles', rolesRouter);
        app.use('/departments', departmentsRouter);
        app.use('/flows', flowRouter);
        app.use('/points', pointRouter);
        app.use('/user-flows', user_flowRouter);
        app.use('/user-points', user_pointsRouter);
    }
});

app.use(session(sessOpts));
app.use(flash());
app.use(passport.initialize());
app.use(passport.session());
app.use(csrf());
app.use(function (err, req, res, next) {
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};
    res.status(err.status || 500);
    res.cookie('XSRF-TOKEN', req.csrfToken());
    res.locals.csrftoken = req.csrfToken();
    next();
});

var file_handler = require("./file_handler.js");

// POST upload handler.
app.post("/upload", function (req, res) {
    file_handler.upload(req, function (err, data) {
        if (err) {
            return res.status(404).end(JSON.stringify(err));
        }
        res.status(200).send(data);
    });
});

// POST delete handler.
app.post('/delete', function (req, res) {
    file_handler._delete(req.body.src, function (err) {
        if (err) {
            return res.status(404).end(JSON.stringify(err));
        }
        res.status(200);
    });
});

app.post('/video_delete', function (req, res) {
    video.delete(req.body.src, function (err) {
        if (err) {
            return res.status(404).end(JSON.stringify(err));
        }
        res.status(200);
    });
});

// for article customize schedule date
app.locals.dateFormat = function (date) {
    if (date !== undefined && date !== null) {
        // return;
        return moment(date).subtract({ hours: 5, minutes: 30 }).format('MMMM DD YYYY, h:mm A');
    } else {
        return ''
    }
}

app.locals.createUpdateDate = function (date) {
    if (date !== undefined && date !== null) {
        return moment(date).format('MMMM DD YYYY, h:mm A')
    } else {
        return ''
    }
}

app.locals.capitalizeFirstLetter = function (string) {
    string = string.charAt(0).toUpperCase() + string.slice(1);
    return string;
}

module.exports = app;