var User = require("../model/users");
module.exports = {
    findOneUser: (query) => {
        return new Promise((resolve, reject) => {
            User.findOne(query)
                .then(user => {
                    if (!user) {
                        var obj = {
                            email: query.email,
                            status: 0 // no email found in db
                        }
                        resolve(obj);
                    } else {
                        user.status = 1 // email found in db
                        resolve(user);
                    }
                })
                .catch(error => {
                    console.log("helper findOneUser error --> ", error);
                    reject(error);
                });
        });
    },

    findUsers: (query) => {
        return new Promise((resolve, reject) => {
            User.find(query)
                .then(email => {
                    resolve(email);
                })
                .catch(error => {
                    console.log("helper findUsers error --> ", error);
                    reject(error);
                })
        })
    },

    findAndUpdate: (con, query) => {
        return new Promise((resolve, reject) => {
            User.findOneAndUpdate(con, query, { new: true })
                .then(user => {
                    resolve(user);
                })
                .catch(err => {
                    reject(err);
                })
        })
    },
    
    findOnePopulate: (query) => {
        return new Promise((resolve, reject) => {
            User.findOne(query)
                .populate('role', { name: 1 })
                .then(result => {
                    resolve(result);
                })
                .catch(error => {
                    reject(error);
                })
        })
    }
}