var Flow = require("../model/flow");
var Point = require("../model/point");
const { compareSync } = require("bcryptjs");
var UserFlow = require('../model/user_flow');
const point = require("../model/point");
const UserPoints = require("../model/user_points");

function string_to_slug(str) {
    str = str.replace(/^\s+|\s+$/g, ''); // trim
    str = str.toLowerCase();

    // remove accents, swap ñ for n, etc
    var from = "àáäâèéëêìíïîòóöôùúüûñç·/_,:;";
    var to = "aaaaeeeeiiiioooouuuunc------";
    for (var i = 0, l = from.length; i < l; i++) {
        str = str.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
    }

    str = str.replace(/[^a-z0-9 -]/g, '') // remove invalid chars
        .replace(/\s+/g, '-') // collapse whitespace and replace by -
        .replace(/-+/g, '-'); // collapse dashes

    return str;
}

module.exports = {
    view: async (req, res, next) => {
        try {
            var query = {};
            var isAdmin;
            // role for check user is admin or not
            var role = await commonFindOneData('Role', { name: 'Admin', deleted_at: null });

            if (JSON.stringify(req.user.role._id) == JSON.stringify(role._id)) {
                isAdmin = true;
            } else {
                // getting flow ids from user_flow for show only particular flow to role wise
                var userFlow = await UserFlow.findOne({ user_id: req.user._id }, { 'flow_details.flow_id': 1 });
                var FlowIDs = [];
                if (userFlow != null) {
                    for (let i = 0; i < userFlow.flow_details.length; i++) {
                        // console.log("userFlow.flow_details :: ", userFlow.flow_details[i].flow_id);
                        FlowIDs.push(userFlow.flow_details[i].flow_id);
                    }
                    query['_id'] = {
                        $in: FlowIDs
                    }
                } else {
                    query['_id'] = {
                        $in: []
                    }
                }

                query['status'] = 'active';
            }
            // console.log("FlowIDs :: ", FlowIDs);

            query['deleted_at'] = null;

            // console.log("query :: ", query);
            var department_filter = '';
            var perPage = req.query.limit ? JSON.parse(req.query.limit) : 5;
            var page = req.query.page ? req.query.page : 1;
            var sort_by = req.query.sort_by ? req.query.sort_by : '_id';
            var sort_type = req.query.sort_type ? req.query.sort_type : 'desc';
            var departments = await commonFindData('Department', { deleted_at: null });
            // for search query 
            if (req.query.search_string) {
                query['title'] = new RegExp(req.query.search_string, "ig");
            }

            // for filter
            if (req.query.filter_status) {
                query['status'] = req.query.filter_status;
                filter_status = req.query.filter_status;
            }


            // for department filter
            if (req.query.department_filter) {
                query['department'] = req.query.department_filter;
                department_filter = req.query.department_filter;
            }
            await Flow.find(query)
                .populate('department')
                .sort([[sort_by, sort_type]])
                .skip((perPage * page) - perPage)
                .limit(perPage)
                .exec(async (err, flow) => {
                    Flow.countDocuments(query).exec(function (err, count) {
                        if (err) return next(err);
                        else {
                            res.render('logedin_index', {
                                what: 'pages/flow/index',
                                title: 'Flow Pages',
                                page_name: 'flows',
                                user: req.user,
                                list: flow,
                                current: page,
                                sort_by: sort_by,
                                sort_type: sort_type,
                                filter_status: req.query.filter_status,
                                search_string: req.query.search_string,
                                pages: Math.ceil(count / perPage),
                                status: Flow.schema.path('status').enumValues,
                                success: req.flash('success'),
                                departments: departments,
                                department_filter: department_filter,
                                csrftoken: req.token,
                                isAdmin: isAdmin
                                // moment: moment,
                            });
                        }
                    })
                })
        } catch (error) {
            console.log("Flow View Error : ", error);
            res.redirect('/');
        }
    },

    create_points: async (req, res, next) => {
        try {
            if (req.body._csrf == undefined || req.body._csrf == null) {
                res.render('index', {
                    what: 'error',
                    title: 'ERROR',
                    message: "CSRF Mismatch",
                    error: {
                        status: 400,
                        stack: 'CSRF'
                    }
                });
            } else {
                req.session.flow = req.body.flow_id;
                res.redirect('/points/create');
            }
        } catch (error) {
            console.log("deartement create error :: ", error);
            res.redirect('/');
        }
    },

    create: async (req, res, next) => {
        try {
            var department = await commonFindData('Department', { status: 'active', deleted_at: null });
            req.active = req.path.split('/')[1];
            res.render('logedin_index', {
                what: 'pages/flow/create',
                title: 'Create Flow',
                csrftoken: req.token,
                user: req.user,
                data: '',
                link: req.active,
                error: '',
                created_by: req.user._id,
                status: Flow.schema.path('status').enumValues,
                department: department,
                page_name: 'flows',
            });
        } catch (error) {
            console.log("deartement create error :: ", error);
            res.redirect('/');
        }
    },

    store: (req, res, next) => {
        try {
            req.body.slug = string_to_slug(req.body.title);
            var flow = new Flow(req.body);
            flow.save()
                .then(result => {
                    req.flash(
                        'success',
                        `Flow Successfully Created`
                    );
                    res.redirect('/flows');
                }).catch(err => {
                    res.redirect('/');
                })
        } catch (error) {
            console.log("flow Create Error : ", error);
            res.redirect('/');
        }
    },

    edit: async (req, res, next) => {
        try {
            var referer = req.headers.referer.split('flows')[1];
            var department = await commonFindData('Department', { status: 'active', deleted_at: null });
            var flow = await commonFindOneData('Flow', { _id: req.params.id, deleted_at: null });
            if (!flow.errorMessage) {
                req.session['redirect'] = referer;
                req.active = req.path.split('/')[2];
                res.render('logedin_index', {
                    what: 'pages/flow/create',
                    title: 'Edit Flow',
                    csrftoken: req.token,
                    user: req.user,
                    data: flow,
                    link: req.active,
                    error: '',
                    created_by: req.user._id,
                    status: Flow.schema.path('status').enumValues,
                    department: department,
                    page_name: 'flows'
                });
            } else {
                req.flash(
                    'info',
                    `Something Went Wron When Edit Flow`
                );
                res.redirect('/flows');
            }

        } catch (error) {
            console.log("Department Edit Error : ", error);
            res.redirect('/');
        }
    },

    update: async (req, res, next) => {
        try {
            req.body.slug = string_to_slug(req.body.title);
            var con = { _id: req.params.id };
            var query = { title: req.body.title, slug: req.body.slug, department: req.body.department, status: req.body.status,description: req.body.description, updated_at: Date.now() }
            var flow = await commonFindOneAndUpdateData('Flow', con, query);
            if (!flow.errorMessage) {
                req.flash(
                    'success',
                    `Flow Successfully Updated`
                );
                if (req.session.redirect) {
                    res.redirect('/flows/' + req.session.redirect);
                } else {
                    res.redirect('/flows');
                }
            } else {
                req.flash(
                    'info',
                    `Something Went Wron When Update Flow`
                );
                res.redirect('/flows');
            }
        } catch (error) {
            console.log("Flow Edit Error : ", error);
            res.redirect('/');
        }
    },

    destroy: async (req, res, next) => {
        try {
            var con = { _id: req.params.id };
            var query = { deleted_at: Date.now() };
            var flow = await commonFindOneAndUpdateData('Flow', con, query);
            if (!flow.errorMessage) {
                req.flash(
                    'success',
                    `Flow Successfully Destory!`
                );
                res.status(200).send(flow)
            } else {
                req.flash(
                    'info',
                    `Something Went Wron When Delete Flow`
                );
                res.redirect('/flows');
            }
        } catch (error) {
            console.log("Flow Edit Error : ", error);
            res.redirect('/');
        }
    },

    status: (req, res, next) => {
        try {
            Flow.findByIdAndUpdate(req.body.id, {
                status: req.body.status,
                updated_at: Date.now()
            }, {
                new: false
            })
                .then(flow => {
                    req.flash(
                        'success',
                        `Flow Status Change Successfully`
                    );
                    res.json({
                        status: true,
                    });
                })
                .catch(error => {
                    res.json({
                        status: false,
                    });
                })
        } catch (error) {
            res.json({
                status: false,
            });
        }
    },

    sorting: async (req, res, next) => {
        try {
            var flow_id = req.params.id;
            var sortable_points = [];
            // var points = await commonFindData('Point', { flow: flow_id, deleted_at: null });
            Point.find({ flow: flow_id, deleted_at: null })
                .sort([['_id', 'asc']])
                .lean()
                .then(async result => {
                    for (let i = 0; i < result.length; i++) {
                        var version = result[i].version.split(".");
                        result[i].children = [];
                        if (version[1] == 0 && version[2] == 0) {
                            sortable_points[version[0]] = result[i];
                        } else {
                            if(sortable_points[version[0]]){
                                if (version[2] == 0) {
                                    sortable_points[version[0]].children[version[1]] = result[i];
                                } else {
                                    sortable_points[version[0]].children[version[1]].children[version[2]] = result[i];
                                }
                            }
                        }
                    }
                    var flow = await commonFindOneData('Flow', { _id: flow_id, deleted_at: null });
                    res.render('logedin_index', {
                        what: 'pages/flow/points_list',
                        title: 'Flow Points',
                        csrftoken: req.token,
                        user: req.user,
                        points: sortable_points,
                        data: flow,
                        error: '',
                        created_by: req.user._id,
                        status: Flow.schema.path('status').enumValues,
                        page_name: 'flows',
                        success: req.flash ? req.flash('success') : ''
                    });
                })
                .catch(error => {
                    console.log("points find error :: ", error);
                    res.redirect('/flows');
                })
        } catch (error) {
            console.log("Flow point Error : ", error);
            res.redirect('/');
        }
    },

    updatePoints: async (req, res, next) => {
        try {
            var sortable = JSON.parse(req.body.points_sorting[0]);
            for (let i = 0; i < sortable.length; i++) {
                var points = await commonFindOneAndUpdateData('Point', { _id: sortable[i].id }, { version: (i + 1) + `.0.0`, updated_at: Date.now() });
                if (sortable[i].children) {
                    var child = sortable[i].children;
                    for (let j = 0; j < child.length; j++) {
                        var childs = await commonFindOneAndUpdateData('Point', { _id: child[j].id }, { version: (i + 1) + `.` + (j + 1) + '.0', updated_at: Date.now() });
                        if (child[j].children) {
                            var childsN = child[j].children
                            for (let n = 0; n < childsN.length; n++) {
                                var childN = await commonFindOneAndUpdateData('Point', { _id: childsN[n].id }, { version: (i + 1) + `.` + (j + 1) + '.' + (n + 1), updated_at: Date.now() });
                            }
                        }
                    }
                }
            }
            res.redirect('/flows');
        } catch (error) {
            console.log("Flow update point Error : ", error);
            res.redirect('/');
        }
    },

    points: async (req, res, next) => {
        try {
            var id = req.params.id;
            var sortable_points = [];
            var points = [];
            var isAdmin;
            // role for check user is admin or not
            var role = await commonFindOneData('Role', { name: 'Admin', deleted_at: null });

            if (JSON.stringify(req.user.role._id) == JSON.stringify(role._id)) {
                isAdmin = true;
            }

            var flow = await commonFindOneData('Flow', { _id: id, deleted_at: null });
            
            if(flow) {
                if(isAdmin || flow.status == 'active') {
                    var user_points = await UserPoints.find({ deleted_at: null, user_id: req.user._id, flow_id: flow._id }, { point_id: 1, status: 1 });

                    for (let i = 0; i < user_points.length; i++) {
                        if (user_points[i].status == 'completed') {
                            points.push(user_points[i].point_id);
                        }
                    }
                    Point.find({ flow: flow._id, deleted_at: null, status: 'active' })
                        .sort([['version', 'asc']])
                        .lean()
                        .then(async result => {
                            for (let i = 0; i < result.length; i++) {
                                var version = result[i].version.split(".");
                                result[i].children = [];
                                if (version[1] == 0 && version[2] == 0) {
                                    if(points.indexOf(result[i]._id) !== -1) {
                                        result[i].checked = true;
                                    }
                                    sortable_points[version[0]] = result[i];
                                } else {
                                    if (version[2] == 0) {
                                        sortable_points[version[0]].children[version[1]] = result[i];
                                    } else {
                                        sortable_points[version[0]].children[version[1]].children[version[2]] = result[i];
                                    }
                                }
                            }
                            res.render('logedin_index', {
                                what: 'pages/flow/listing',
                                title: flow.title,
                                csrftoken: req.token,
                                user: req.user,
                                points: sortable_points,
                                flow: flow,
                                isAdmin: isAdmin,
                                error: '',
                                created_by: req.user._id,
                                status: Flow.schema.path('status').enumValues,
                                page_name: 'flows'
                            });
                        })
                        .catch(error => {
                            console.log("points find error :: ", error);
                            res.redirect('/flows');
                        })
                } else {
                    req.flash(
                        'error',
                        `Flow not found`
                    );
                    res.redirect('/');
                }
            } else {
                req.flash(
                    'error',
                    `Flow not found`
                );
                res.redirect('/');
            }
        } catch (error) {
            console.log("Flow point Error : ", error);
            res.redirect('/');
        }
    },
}