const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment-fix');

const Point = new Schema({
    title: {
        type: String,
    },
    media_key: {
        type: String,
        required: true,
        unique: true
    },
    hints: {
        type: String,
    },
    content: {
        type: String,
    },
    version: {
        type: String,
    },
    status: {
        type: String,
        enum: ['pending', 'active', 'inactive'],
        default: 'pending'
    },
    sort: {
        type: Number,
        default: 0
    },
    flow: {
        type: Number,
        ref: 'Flow',
        required: true
    },
    created_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    updated_at: {
        type: Date,
        required: true,
        default: Date.now(),
    },
    deleted_at: {
        type: Date,
        default: null,
    }
}, {
    collection: 'points'
});

Point.plugin(autoIncrement.plugin, {
    model: 'Point',
    startAt: 1,
    incrementBy: 1
});

module.exports = mongoose.model('Point', Point);