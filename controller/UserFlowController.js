var UserHelper = require('../helpers/user');
var bcrypt = require('bcryptjs');
var salt = bcrypt.genSaltSync(10);
var User = require('../model/users');
var UserPoint = require('../model/user_points');
var UserFlow = require('../model/user_flow');
var mongoose = require("mongoose");
var moment = require('moment');
var fs = require("fs");
const user = require('../helpers/user');

module.exports = {
    destroy: async (req, res, next) => {
        try {
            if (req.body._csrf == undefined || req.body._csrf == null) {
                res.render('index', {
                    what: 'error',
                    title: 'ERROR',
                    message: "CSRF Mismatch",
                    error: {
                        status: 400,
                        stack: 'CSRF'
                    }
                });
            } else {
                var flow_details = [];
                var obj = {
                    deleted_at: Date.now(),
                }
                var userFlow = await commonFindOneData('UserFlow', { user_id: req.body.user_id });
                if (userFlow && userFlow.flow_details) {
                    var index = userFlow.flow_details.findIndex(x => x.flow_id.toString() == req.body.flow_id.toString())
                    flow_details = userFlow.flow_details.splice(index, 1);
                    var updateUserFlow = await commonFindOneAndUpdateData('UserFlow', { user_id: req.body.user_id, deleted_at: null }, { flow_details: userFlow.flow_details });
                    if (!updateUserFlow.errorMessage) {
                        UserPoint.update({ flow_id: req.body.flow_id }, { status: 'uncompleted', updated_at: Date.now() }, { multi: true })
                            .then(result => {
                                // console.log("result --------> ", result)
                            })
                            .catch(error => {
                                // console.log("error --------> ", error)
                            })
                        req.flash(
                            'success',
                            `Successfully Remove User Flow`
                        );
                        res.redirect('/user-flows/' + req.body.user_id);
                    } else {
                        console.log("userPOints error ---------> ");
                        req.flash(
                            'Info',
                            `Error While Delete User Flow`
                        );
                        res.redirect('/user-flows/' + req.body.user_id);
                    }
                } else {
                    console.log("else case 111-----------");
                    res.redirect('/')
                }
            }
        } catch (error) {
            console.log("destroy error :: ", error);
            res.redirect('/');
        }
    },

    flow_status: async (req, res, next) => {
        try {
            var flowList = [];
            // console.log("flowList --------> ", flowList);
            var userFlow = await UserFlow.findOne({ user_id: req.params.id }, { 'flow_details.flow_id': 1 });
            var FlowIDs = [];
            var uFlow = null
            if (userFlow != null && userFlow.flow_details.length > 0) {
                for (let i = 0; i < userFlow.flow_details.length; i++) {
                    FlowIDs.push(userFlow.flow_details[i].flow_id);
                }
                if (!userFlow.errorMessage && FlowIDs != null) {
                    uFlow = FlowIDs
                }
            }
            if (req.user.role.name === 'Admin') {
                UserFlow.findOne({ user_id: req.params.id, deleted_at: null }, { 'flow_details': 1, 'user_id': 1, 'status': 1, 'updated_at': 1 })
                    .populate('user_id')
                    .populate('flow_details.flow_id')
                    .lean()
                    .exec(async (err, flow) => {
                        if (flow != null && flow.flow_details.length > 0) {
                            for (let i = 0; i < flow.flow_details.length; i++) {
                                // get start flow time by user
                                await UserPoint.find({ user_id: req.params.id, flow_id: flow.flow_details[i].flow_id, status: 'completed' }).sort([['created_at', 'desc']]).lean()
                                    .then(userpoint => {
                                        // console.log("`userpoint` :: ", userpoint);
                                        if (userpoint.length) {
                                            for (let j = 0; j < userpoint.length; j++) {
                                                flow.flow_details.findIndex(x => {
                                                    if (x.flow_id._id.toString() == userpoint[j].flow_id.toString()) {
                                                        if (new Date(userpoint[j].created_at) < new Date(userpoint[j].updated_at)) {
                                                            x.start_time = userpoint[j].updated_at;
                                                        } else {
                                                            x.start_time = userpoint[j].created_at;
                                                        }
                                                    }
                                                })
                                            }
                                        }
                                    });
                                // get complete flow time by user
                                if (flow.flow_details[i].status == 'completed') {
                                    await UserPoint.find({ user_id: req.params.id, flow_id: flow.flow_details[i].flow_id }).sort([['updated_at', 'asc']]).lean()
                                        .then(userpoint => {
                                            for (let j = 0; j < userpoint.length; j++) {
                                                flow.flow_details.findIndex(x => {
                                                    if (x.flow_id._id.toString() == userpoint[j].flow_id.toString()) {
                                                        x.end_time = userpoint[j].updated_at;
                                                    }
                                                })
                                            }
                                        });
                                }
                            }

                            flowList = await commonFindData('Flow', { _id: { '$nin': FlowIDs }, deleted_at: null, status: 'active' });

                            res.render('logedin_index', {
                                what: 'pages/users/user_flow',
                                flowList: flowList,
                                userFlows: uFlow,
                                user_id: req.params.id,
                                flows: flow.flow_details,
                                title: 'User\'s Flow',
                                user: req.user,
                                csrftoken: req.token,
                                success: req.flash('success'),
                                page_name: 'users',
                                moment: moment
                            });
                        } else {
                            flowList = await commonFindData('Flow', { deleted_at: null, status: 'active' });

                            res.render('logedin_index', {
                                what: 'pages/users/user_flow',
                                flowList: flowList,
                                userFlows: uFlow,
                                user_id: req.params.id,
                                flows: null,
                                title: 'User\'s Flow',
                                user: req.user,
                                csrftoken: req.token,
                                success: req.flash('success'),
                                page_name: 'users',
                                moment: moment
                            });
                        }
                    });
            } else {
                res.render('logedin_index', {
                    what: 'pages/403',
                    title: 'Error',
                    message: 'You Have No Rights To View Points of User',
                    href: req.baseUrl,
                    user: req.user,
                    csrftoken: req.token,
                    page_name: 'users',
                });
            }
        } catch (error) {
            console.log("paging error : ", error);
            res.redirect('/');
        }
    },

    flow_assign: async (req, res, next) => {
        try {
            var user_flow = await UserFlow.findOne({ user_id: req.body.user_id });
            if(user_flow == null) {
                user_flow = await new UserFlow({ user_id: req.body.user_id }).save();
            }

            var flow = [];
            var user_flow = await UserFlow.findOne({ user_id: req.body.user_id });
            var index = 0;
            if (user_flow && user_flow.flow_details) {
                for (let i = 0; i < user_flow.flow_details.length; i++) {
                    if (user_flow.flow_details[i].flow_id.toString() == req.body.flow.toString()) {
                        index = 1
                    }
                }
                if (index == 0) {
                    flow = user_flow.flow_details;
                    flow.push({ status: 'uncompleted', flow_id: req.body.flow });
                }
            } else {
                flow = [{ status: 'uncompleted', flow_id: req.body.flow }];
            }
            var user_flow = await UserFlow.findOneAndUpdate({ user_id: req.body.user_id }, { flow_details: flow });
            res.redirect('/user-flows/' + req.body.user_id);
        } catch (error) {
            console.log("paging error : ", error);
            res.redirect('/');
        }
    }
}