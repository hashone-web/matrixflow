var request = require('request')
module.exports = {
    sendNotifications: (appId, restApiKey, data) => {
        // return;
        return new Promise((resolve, reject) => {
            request(
                {
                    method: 'POST',
                    uri: 'https://onesignal.com/api/v1/notifications',
                    headers: {
                        "authorization": "Basic " + restApiKey,
                        "content-type": "application/json"
                    },
                    json: true,
                    body: {
                        'app_id': appId,
                        'contents': { en: data.headline },
                        'included_segments': ["Active Users"],
                        'data': {
                            'type': 'article',
                            'id': data.id,
                        }
                    },
                },
                function (error, response, body) {
                    if (!body.errors) {
                        // console.log(body);
                        resolve(body);
                    } else {
                        console.error('Error:', body.errors);
                        reject(body.errors);
                    }
                }
            );
        })
    }
}