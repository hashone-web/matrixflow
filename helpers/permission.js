var Permission = require("../model/permissons");

module.exports = {
    getAllPermission: () => {
        return new Promise((resolve, reject) => {
            Permission.find({ deleted_at: null })
                .then(permission => {
                    resolve(permission);
                })
                .catch(err => {
                    reject(err);
                })
        })
    },

    getPermissionById: (query) => {
        return new Promise((resolve, reject) => {
            Permission.find(query)
                .then(permission => {
                    resolve(permission);
                })
                .catch(err => {
                    reject(err);
                });
        });
    },

    findAndUpdate: (con, query) => {
        return new Promise((resolve, reject) => {
            Permission.findOneAndUpdate(con, query)
                .then(permission => {
                    resolve(permission);
                })
                .catch(err => {
                    reject(err);
                });
        });
    },
}