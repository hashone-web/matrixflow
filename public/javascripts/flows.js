$(document).ready(function () {
    new FroalaEditor('#description', {
        toolbarButtons: [['bold', 'italic', 'underline']]
    });
});