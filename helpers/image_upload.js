const fs = require('fs');
const path = require('path');
const mkdirp = require('mkdirp')
const Jimp = require('jimp');
const make_random = require('./make_random');
var base64ToImage = require('base64-to-image');

module.exports = {
    userFile: (base64Str, userId) => {
        return new Promise((function (resolve, reject) {
            var ex = base64Str.substring(base64Str.indexOf('/') + 1, base64Str.indexOf(';base64'));
            // console.log("ex:: ", ex);
            var newName = make_random.random(12);
            dir = path.join(appRoot, 'public/storage/users/' + userId + '/image/');
            if (!fs.existsSync(dir)) {
                mkdirp.sync(dir, { recursive: true });
            }
            var optionalObj = { 'fileName': newName, 'type': ex };
            var imageInfo = base64ToImage(base64Str, dir, optionalObj);
            if (imageInfo) {
                var promises = [
                    resizeImage(dir, newName + '.' + imageInfo.imageType, 64),
                    resizeImage(dir, newName + '.' + imageInfo.imageType, 128),
                    resizeImage(dir, newName + '.' + imageInfo.imageType, 256),
                ];

                Promise.all(promises).then(function () {
                    resolve(newName + '.' + imageInfo.imageType);
                }).catch(function (err) {
                    reject(err);
                });
            } else {
                resolve(newName + '.' + imageInfo.imageType);
            }
        }))
    },
}

function resizeImage(source_path, file_name, size) {
    return new Promise(function (resolve, reject) {
        const destination_path = path.join(source_path, size.toString());
        if (!fs.existsSync(destination_path)) {
            mkdirp.sync(destination_path, { recursive: true });
        } else {
            deleteFolderRecursive(destination_path);
        }

        Jimp.read(path.join(source_path, file_name), async (err, data) => {
            if (err) reject(err);
            await data
                .resize(Jimp.AUTO, size) // resize
                .quality(80) // set JPEG quality
                .write(path.join(destination_path, file_name)); // save
            resolve();
        });
    })
}

var deleteFolderRecursive = function (final_path) {
    if (fs.existsSync(final_path)) {
        fs.readdirSync(final_path).forEach(function (file, index) {
            var curPath = path.join(final_path, file);
            fs.unlinkSync(curPath); // delete file
        });
        fs.rmdirSync(final_path);
    }
};
