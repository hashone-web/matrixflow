const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const autoIncrement = require('mongoose-auto-increment-fix');

const opts = { toJSON: { virtuals: true } };

const User = new Schema({
    name: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    mobile_no: {
        type: String,
    },
    status: {
        type: String,
        enum: ['pending', 'active', 'blocked', 'rejected'],
        default: 'pending'
    },
    type: {
        type: String,
        enum: ['admin', 'application'],
        default: 'admin'
    },
    password: {
        type: String,
    },
    content_password: {
        type: String,
        default: '123456'
    },
    profile_picture: {
        type: String,
        default: null
    },
    role: {
        type: Schema.Types.ObjectId,
        ref: 'Role',
    },
    created_by: {
        type: Number,
        ref: 'User'
    },
    updated_by: {
        type: Number,
        ref: 'User'
    },
    deleted_at: {
        type: Date,
        default: null,
    }
}, {
    timestamps: {
        createdAt: 'created_at',
        updatedAt: 'updated_at'
    },
    collection: 'users'
}, opts);


User.virtual('image_full_path').get(function() {
    return 'users/' + this._id + '/image/' + this.profile_picture;
});

User.plugin(autoIncrement.plugin, {
    model: 'User',
    startAt: 1,
    incrementBy: 1
});

module.exports = mongoose.model('User', User);