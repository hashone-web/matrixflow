var express = require('express');
var router = express.Router();
var DepartmentController = require('../controller/DepartmentController');
var auth = require('../config/auth');

// department/
router.get('/', [auth.isLoggedIn, auth.csrfConfig], DepartmentController.view);

// department/create
router.get('/create', [auth.isLoggedIn, auth.csrfConfig], DepartmentController.create);

// department/create
router.post('/', [auth.isLoggedIn], DepartmentController.store);

// department/status
router.post('/status', [auth.isLoggedIn], DepartmentController.status);

// department/:id/edit
router.get('/:id/edit', [auth.isLoggedIn, auth.csrfConfig], DepartmentController.edit);

// department/:id
router.post('/:id', [auth.isLoggedIn], DepartmentController.update);

// department/:id/destory
router.post('/:id/destroy', [auth.isLoggedIn], DepartmentController.destroy);


module.exports = router;
