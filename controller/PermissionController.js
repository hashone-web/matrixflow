var Permission = require("../model/permissons");
var PermissionHelper = require('../helpers/permission');

module.exports = {
    view: async (req, res, next) => {
        try {
            var Helper = await PermissionHelper.getAllPermission();
            if (Helper.length > 0) {
                res.send(Helper);
            } else {
                res.send("No Permission found");
            }
        } catch (error) {
            console.log("Permission View Error : ", error);
            res.send(error);
        }
    },

    create: (req, res, next) => {
        try {
            var permission = new Permission(req.body);
            permission.save()
                .then(result => {
                    res.send(result);
                }).catch(err => {
                    console.log("Permission Create Error : ", err);
                    res.send(err);
                })
        } catch (error) {
            console.log("Create permission Error : ", error);
            res.send("Permission create error : ", error);
        }
    },

    edit: async (req, res, next) => {
        try {
            var query = { _id: req.params.id }
            var Helper = await PermissionHelper.getPermissionById(query);
            if (Helper) {
                res.send(Helper);
            } else {
                res.send("No Permission found");
            }
        } catch (error) {
            res.send("Permission edit error : ", error);
        }
    },

    update: async (req, res, next) => {
        try {
            var con = { _id: req.params.id };
            var query = { name: req.body.name, updated_at: Date.now() }
            var Helper = await PermissionHelper.findAndUpdate(con, query);
            if (Helper) {
                res.send(Helper);
            } else {
                res.send("No Permission updated");
            }
        } catch (error) {
            console.log("Permisson Update Error : ", error);
            res.send(error);
        }
    },

    destroy: async (req, res, next) => {
        try {
            var con = { _id: req.params.id };
            var query = { deleted_at: Date.now() };
            var Helper = await PermissionHelper.findAndUpdate(con, query);
            if (Helper) {
                res.send(Helper);
            } else {
                res.send("No Permission updated");
            }
        } catch (error) {
            console.log("Permission Destory Error : ", error);
            res.send(error);
        }
    }
}