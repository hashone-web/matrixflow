function makeRandom(length) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    // console.log(result);
    return result;
}

module.exports = {
    decodeToken: (req, res, next) => {
        if (req.body.token || req.query.token) {
            var token = req.body.token ? req.body.token : req.query.token;
            let buff = new Buffer.from(token, 'base64');
            token = buff.toString('ascii');
            token = token.substr(5);
            token = token.substr(0, token.length - 6);
            var token_first_five = token.substr(0, 5);
            var token_after_nine = token.substr(9);
            var final_token = token_first_five + token_after_nine;
            let finalToken = new Buffer.from(final_token, 'base64');
            finalToken = finalToken.toString('ascii');
            if (finalToken == 'com.current.affairs.general.knowledge.and.all.exam.preparation') {
                next();
            } else {
                res.status(200).send({
                    date: new Date(),
                    status: false,
                    data: 'Invalid Token Pass!',
                })
            }
        } else {
            res.status(200).send({
                date: new Date(),
                status: false,
                data: 'Token Is Not Pass!',
            })
        }
    },

    encodeToken: (token) => {
        // console.log("-------------------------------------");
        if (req.body.token || req.query.token) {
            var token = req.body.token ? req.body.token : req.query.token;
            token = new Buffer.from(token);
            token = token.toString('base64');
            // console.log(token);
            var token_first_five = token.substr(0, 5);
            // console.log("token_first_five :: ", token_first_five);
            var token_after_five = token.substr(5);
            // console.log("token_after_nine :: ", token_after_five);
            var final_token = makeRandom(5) + token_first_five + makeRandom(4) + token_after_five + makeRandom(6);
            // console.log("final_token :: ", final_token);
            var finalToken = new Buffer.from(final_token);
            finalToken = finalToken.toString('base64');
            // console.log(finalToken);
            if (finalToken) {
                next();
            } else {
                res.status(200).send({
                    date: new Date(),
                    status: false,
                    data: 'Something Went Wrong While Encode Token!',
                })
            }

        } else {
            res.status(200).send({
                date: new Date(),
                status: false,
                data: 'Token Is Not Pass!',
            })
        }
    }
}